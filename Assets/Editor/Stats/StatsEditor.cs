using StatSystem;
using UnityEditor;

namespace Editor
{
    [CustomEditor(typeof(Stats))]
    public abstract class StatsEditor : UnityEditor.Editor
    {
        protected abstract Stats MyTarget { get; }

        public override void OnInspectorGUI()
        {
            Initialize();
            PrintStats();
            PrintModifiers();
            PrintMultipliers();

            DrawDefaultInspector();
        }

        protected abstract void Initialize();

        private void PrintStats()
        {
            EditorGUILayout.HelpBox($"DPS : {MyTarget.AttackSpeed * MyTarget.AttackDamage}", MessageType.Info);
            EditorGUILayout.LabelField("Attack Damage", $" {MyTarget.AttackDamage}");
            EditorGUILayout.LabelField("Attack Range", $" {MyTarget.AttackRange}");
            EditorGUILayout.LabelField("Attack Speed", $" {MyTarget.AttackSpeed}");
            EditorGUILayout.LabelField("Move Speed", $" {MyTarget.MoveSpeed}");
        }

        private void PrintModifiers()
        {
            EditorGUILayout.HelpBox("Modifiers", MessageType.Info);
            EditorGUILayout.LabelField("Attack Damage", $" {MyTarget.modifiers.AttackDamageModifier}");
            EditorGUILayout.LabelField("Attack Range", $" {MyTarget.modifiers.AttackRangeModifier}");
            EditorGUILayout.LabelField("Attack Speed", $" {MyTarget.modifiers.AttackSpeedModifier}");
            EditorGUILayout.LabelField("Move Speed", $" {MyTarget.modifiers.MoveSpeedModifier}");
        }
        
        private void PrintMultipliers()
        {
            EditorGUILayout.HelpBox("Modifiers", MessageType.Info);
            EditorGUILayout.LabelField("Attack Damage", $" {MyTarget.multipliers.AttackDamageMultiplier}");
            EditorGUILayout.LabelField("Attack Range", $" {MyTarget.multipliers.AttackRangeMultiplier}");
            EditorGUILayout.LabelField("Attack Speed", $" {MyTarget.multipliers.AttackSpeedMultiplier}");
            EditorGUILayout.LabelField("Move Speed", $" {MyTarget.multipliers.MoveSpeedMultiplier}");
        }
        
    }
}
