﻿using StatSystem;
using UnityEditor;

namespace Editor
{
    [CustomEditor(typeof(AlienStats))]
    public class AlienStatsEditor : StatsEditor
    {
        protected override Stats MyTarget => _myTarget;
        private AlienStats _myTarget;

        protected override void Initialize()
        {
            _myTarget = (AlienStats) target;
        }
        
    }
}