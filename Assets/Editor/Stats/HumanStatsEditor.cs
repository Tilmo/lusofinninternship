﻿using StatSystem;
using UnityEditor;

namespace Editor
{
    [CustomEditor(typeof(HumanStats))]
    public class HumanStatsEditor : StatsEditor
    {
        protected override Stats MyTarget => _myTarget;
        private HumanStats _myTarget;

        protected override void Initialize()
        {
            _myTarget = (HumanStats) target;
        }
        
    }
}