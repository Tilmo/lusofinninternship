﻿using StatSystem;
using UnityEditor;

namespace Editor
{
    [CustomEditor(typeof(DebugStats))]
    public class DebugStatsEditor : StatsEditor
    {
        protected override Stats MyTarget => _myTarget;
        private DebugStats _myTarget;

        protected override void Initialize()
        {
            _myTarget = (DebugStats) target;
        }
        
        
        
    }
}