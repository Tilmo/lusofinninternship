﻿using System;
using Resources;
using UnityEngine;

namespace Serialization
{
    public class SaveManager : MonoBehaviour
    {
        [ContextMenu("Save SaveData")]
        public void Save()
        {
            SaveData.Current.worldMapSave = WorldMap.Blueprints.SimpleMapManager.Instance.map;
            SaveData.Current.humansAmount = ResourceManager.Instance.resourceWorldSession.HumanAmount;
            SaveData.Current.metalAmount = ResourceManager.Instance.resourceWorldSession.StartingMetal;
            SaveData.Current.researchAmount = ResourceManager.Instance.resourceWorldSession.ResearchAmount;
            SerializationManager.Save("Save", SaveData.Current);
        }
        
        public void Load()
        {
            SaveData.Current.SetSaveData((SaveData)SerializationManager.Load(Application.dataPath + "/saves/Save.save") );
        }

        [ContextMenu("Reset Save")]
        public void DefaultSaveValues()
        {
            SaveData.Current.humansAmount = 100;
            SaveData.Current.researchAmount = 0;
            SaveData.Current.metalAmount = 150;

            SaveData.Current.worldMapSave = null;
            
            SerializationManager.Save("Save", SaveData.Current);
        }
    }
}