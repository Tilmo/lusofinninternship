﻿using WorldMap.Blueprints;

namespace Serialization
{
    [System.Serializable]
    public class SaveData
    {
        public static SaveData Current => _current ??= new SaveData();
        private static SaveData _current;

        public void SetSaveData(SaveData data) => _current = data;

        public int humansAmount = 100;
        public int researchAmount = 0;
        public int metalAmount = 100;

        public MapTemplateLayer[] worldMapSave;
    }
}