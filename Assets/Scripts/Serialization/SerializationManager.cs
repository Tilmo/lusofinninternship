using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Serialization
{
    public class SerializationManager : MonoBehaviour
    {
        private static string _path;
        public static bool Save(string saveName, object saveData)
        {
            BinaryFormatter formatter = GetBinaryFormatter();

            if (!Directory.Exists(Application.dataPath + "/saves"))
            {
                Directory.CreateDirectory(Application.dataPath + "/saves");
            }

            _path = Application.dataPath + "/saves/" + saveName + ".save";

            FileStream file = File.Create(_path);
            formatter.Serialize(file, saveData);
        
            file.Close();

            return true;
        }

        public static object Load(string path)
        {
            if (!File.Exists(path))
            {
                return null;
            }

            BinaryFormatter formatter = GetBinaryFormatter();

            FileStream file = File.Open(path, FileMode.Open);

            try
            {
                object save = formatter.Deserialize(file);
                file.Close();
                return save;
            }
            catch
            {
                Debug.Log("Failed to load file");
                file.Close();
                return null;
            }
        }

        private static BinaryFormatter GetBinaryFormatter()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            return formatter;
        }
    }
}
