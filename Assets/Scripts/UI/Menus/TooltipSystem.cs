using UnityEngine;

namespace UI.Menus
{
    public class TooltipSystem : MonoBehaviour
    {
        public Tooltip tooltip;
        private static TooltipSystem _current;
        private void Awake()
        {
            _current = this;
        }
        public static void Show(string content, string header = "")
        {
            _current.tooltip.SetText(content, header);
            _current.tooltip.gameObject.SetActive(true);
        }
        public static void Hide()
        {
            _current.tooltip.gameObject.SetActive(false);
        }
    }
}
