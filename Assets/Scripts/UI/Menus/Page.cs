﻿using System.Collections;
using UnityEngine;

namespace UI.Menus
{
    public class Page : MonoBehaviour
    {
        public PageState TargetState { get; private set; }

        public bool useAnimation;
        public PageType type;

        private Animator _animator;

        public void Animate(bool on)
        {
            if (useAnimation)
            {
                StopCoroutine(nameof(AwaitAnimation));
                StartCoroutine(AwaitAnimation(on));
            }
            else
            {
                if (!on)
                {
                    gameObject.SetActive(false);
                }
            }
        }
        
        

        private IEnumerator AwaitAnimation(bool on)
        {
            TargetState = on ? PageState.On : PageState.Off;

            while (!_animator.GetCurrentAnimatorStateInfo(0).IsName(TargetState.ToString()))
            {
                yield return null;
            }

            while (_animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
            {
                yield return null;
            }

            TargetState = PageState.None;

            if (!on)
            {
                gameObject.SetActive(false);
            }
            
            yield return null;
        }
        
    }
}