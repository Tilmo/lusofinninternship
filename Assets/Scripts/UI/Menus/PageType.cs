﻿namespace UI.Menus
{
    public enum PageType
    {
        None, 
        Loading,
        Menu,
        GameOverlay,
        AbilityMenu,
        BuildingMenu,
        Settings,
        WorldMenu,
        NodeInfo
    }
}