﻿using System;
using System.Collections;
using UnityEngine;

namespace UI.Menus
{
    public class PageController : MonoBehaviour
    {
        public static PageController Instance { get; private set; }

        public PageType entryPage;
        
        private Page[] _pages;
        
        private Hashtable _pageTypeToPage;


        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
                _pageTypeToPage = new Hashtable();

                _pages = gameObject.GetComponentsInChildren<Page>(true);
                RegisterAllPages();
                
                if(entryPage != PageType.None)
                    TurnPageOn(entryPage);
                
            }
        }
        
        public void TurnPageOn(PageType type)
        {
            if(type == PageType.None) return;
            if (!PageExists(type))  return;

            var page = GetPage(type);
            page.gameObject.SetActive(true);
            page.Animate(true);
        }

        private static PageType TypeFromString(string pageTypeString) => (PageType) Enum.Parse(typeof(PageType), pageTypeString, true);
        
        public void TurnPageOff(string off)
        {
            PageType type = TypeFromString(off);

            
            if(!PageExists(type)) return;
            
            TurnPageOff(type);
        }
        public void TurnPageOn(string pageTypeString)
        {
            PageType type = TypeFromString(pageTypeString);
            
            if(!PageExists(type)) return;
            
            TurnPageOn(type);
        }

        public void TurnPageOff(PageType off, PageType on=PageType.None, bool waitForExit = false)
        {
            if(off == PageType.None) return;
            
            if (!PageExists(off))  return;

            var offPage = GetPage(off);
            
            if (offPage.gameObject.activeSelf)
            {
                offPage.Animate(false);
            }

            if (on != PageType.None)
            {
                var onPage = GetPage(on);

                if (waitForExit)
                {
                    StartCoroutine(WaitForPageExit(onPage, offPage));
                }
                else
                {
                    TurnPageOn(on);
                }
            }
        }


        private Page GetPage(PageType type)
        {
            if (!PageExists(type)) return null;
            
            return (Page) _pageTypeToPage[type];
        }

        private void RegisterAllPages()
        {
            foreach (Page page in _pages)
            {
                RegisterPage(page);
            }
        }
        
        private void RegisterPage(Page page)
        {
            if(PageExists(page.type)) return;
            
            _pageTypeToPage.Add(page.type, page);
        }

        private bool PageExists(PageType type)
        {
            return _pageTypeToPage.ContainsKey(type);
        }

        private IEnumerator WaitForPageExit(Page on, Page off)
        {
            while (off.TargetState != PageState.None)
            {
                yield return null;
            }
            
            TurnPageOn(on.type);
        }


    }
}