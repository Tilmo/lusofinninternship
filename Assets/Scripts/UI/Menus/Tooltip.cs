using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace UI.Menus
{
    [ExecuteInEditMode]
    public class Tooltip : MonoBehaviour
    {
        public TextMeshProUGUI headerField;
        public TextMeshProUGUI contentField;
        public LayoutElement layoutElement;

        public RectTransform rectTransform;

        public int characterWrapLimit;

        private void Update()
        {
            Vector2 position = Mouse.current.position.ReadValue();

            position += Vector2.down * 50;
            transform.position = position;

            float pivotX = position.x / Screen.width;
            float pivotY = position.y / Screen.height;
            
            rectTransform.pivot = new Vector2(pivotX, pivotY);
            if(!Application.isEditor) return;
        
            int headerLenght = headerField.text.Length;
            int contentLenght = contentField.text.Length;

            layoutElement.enabled =
                (headerLenght > characterWrapLimit || contentLenght > characterWrapLimit);
        }

        public void SetText(string content, string header = "")
        {
            if (string.IsNullOrEmpty(header))
            {
                headerField.gameObject.SetActive(false);
            }
            else
            {
                headerField.gameObject.SetActive(true);
                headerField.text = header;
            }

            contentField.text = content;
        
            int headerLenght = headerField.text.Length;
            int contentLenght = contentField.text.Length;

            layoutElement.enabled =
                (headerLenght > characterWrapLimit || contentLenght > characterWrapLimit);
        }
    }
}
