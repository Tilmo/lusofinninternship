﻿using Core;
using UnityEngine;

namespace UI
{
    public class AbilitySelectButtonUI : MonoBehaviour
    {
        public ActiveToggleUI activeToggleUI;

        public void Toggle()
        {
            if (GameManager.Game.AbilityController.skillPoints > 0)
            {
                activeToggleUI.Toggle();
            }
        }

        public void CheckSkillPoints()
        {
            if (GameManager.Game.AbilityController.skillPoints <= 0)
            {
                activeToggleUI.Toggle();
            }
        }
    }
}