using System;
using Resources;
using TMPro;
using UnityEngine;

namespace UI
{
    public class UpdateHumansUI : MonoBehaviour
    {
        private TMP_Text _text;

        private void Awake()
        {
            _text = GetComponent<TMP_Text>();
        }
        
        private void OnEnable()
        {
            ResourceManager.OnHumansAmountChanged += ResourceManagerOnOnHumansAmountChanged;
        }

        private void ResourceManagerOnOnHumansAmountChanged(object sender, EventArgs e)
        {
            _text.text =
                $"Humans - {ResourceManager.Instance.HumanAmount.ToString()}({ResourceManager.Instance.HumansUsed.ToString()})";
        }
    
        private void OnDisable()
        {
            ResourceManager.OnHumansAmountChanged -= ResourceManagerOnOnHumansAmountChanged;
        }
    }
}
