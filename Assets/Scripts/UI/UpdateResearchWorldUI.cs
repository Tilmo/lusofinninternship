﻿using System;
using Resources;
using TMPro;
using UnityEngine;

namespace UI
{
    public class UpdateResearchWorldUI : MonoBehaviour
    {
        private TMP_Text _text;

        private void Awake()
        {
            _text = GetComponent<TMP_Text>();
        }
        
        private void OnEnable()
        {
            ResourceManager.OnResearchAmountChangedWorld += ResourceManagerOnOnResearchAmountChanged;
        }

        private void ResourceManagerOnOnResearchAmountChanged(object sender, EventArgs e)
        {
            
            _text.text = $"Research - {ResourceManager.Instance.ResourceWorldSession.ResearchAmount.ToString()}";
        }

        private void OnDisable()
        {
            ResourceManager.OnResearchAmountChangedWorld -= ResourceManagerOnOnResearchAmountChanged;
        }
    }
}