using UnityEngine;

namespace UI
{
    public class ActiveToggleUI : MonoBehaviour
    {
        public void Toggle()
        {
            gameObject.SetActive( !gameObject.activeSelf );
        }
    }
}
