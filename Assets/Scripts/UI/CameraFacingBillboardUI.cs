using UnityEngine;

namespace UI
{
    public class CameraFacingBillboardUI : MonoBehaviour
    {
        private UnityEngine.Camera _camera;

        private void Awake()
        {
            _camera = new TDCamera.GameCamera().Camera;
        }

        private void LateUpdate()
        {
            var rotation = _camera.transform.rotation;
        
            transform.LookAt(transform.position + rotation * Vector3.forward,
                rotation * Vector3.up);
        }
    }
}
