﻿using Core;
using Humans;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class AttackSliderUI : MonoBehaviour
    {
        private Turret _turret;
        private Slider _slider;
        private Image _image;
        
        private void Start()
        {
            _turret ??= transform.root.GetComponent<Turret>();
            _slider = GetComponent<Slider>();
            _image = GetComponentInChildren<Image>();
            
            if(_turret.WeaponDamageType == DamageType.ArmorPiercing)
                _image.color = Color.black;
            
            _turret.OnWeaponFire += TurretOnOnWeaponFire; 
        }

        private void TurretOnOnWeaponFire()
        {
            _slider.value = Mathf.FloorToInt(Mathf.Clamp(_turret.WeaponTimerPercentage * 100, 0, 100));
        }
    }
}