﻿using System;
using Resources;
using TMPro;
using UnityEngine;

namespace UI
{
    public class UpdateResearchUI : MonoBehaviour
    {
        private TMP_Text _text;

        private void Awake()
        {
            _text = GetComponent<TMP_Text>();
        }
        
        private void OnEnable()
        {
            ResourceManager.OnResearchAmountChanged += ResourceManagerOnOnResearchAmountChanged;
        }

        private void ResourceManagerOnOnResearchAmountChanged(object sender, EventArgs e)
        {
            
            _text.text = $"Research - {ResourceManager.Instance.ResearchAmount.ToString()}";
        }

        private void OnDisable()
        {
            ResourceManager.OnResearchAmountChanged -= ResourceManagerOnOnResearchAmountChanged;
        }
    }
}