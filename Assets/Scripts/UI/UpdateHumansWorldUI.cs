﻿using System;
using Resources;
using TMPro;
using UnityEngine;

namespace UI
{
    public class UpdateHumansWorldUI : MonoBehaviour
    {
        private TMP_Text _text;

        private void Awake()
        {
            _text = GetComponent<TMP_Text>();
        }
        
        private void OnEnable()
        {
            ResourceManager.OnHumansAmountChangedWorld += ResourceManagerOnOnHumansAmountChanged;
        }

        private void ResourceManagerOnOnHumansAmountChanged(object sender, EventArgs e)
        {
            _text.text =
                $"Humans - {ResourceManager.Instance.ResourceWorldSession.HumanAmount.ToString()}";
        }
    
        private void OnDisable()
        {
            ResourceManager.OnHumansAmountChangedWorld -= ResourceManagerOnOnHumansAmountChanged;
        }
    }
    
    
}
