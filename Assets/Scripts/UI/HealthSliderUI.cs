using Health;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HealthSliderUI : MonoBehaviour
    {
        private IDamageable _hitPoints;
        private Slider _slider;
        private TMP_Text _text;
        private Image _image;

        private void Start()
        {
            _hitPoints ??= transform.root.GetComponent<IDamageable>();

            _text = GetComponentInChildren<TMP_Text>();
            _slider = GetComponent<Slider>();

            _image = transform.GetChild(1).GetComponent<Image>();

            if (_hitPoints is ArmoredHitPoints)
            {
                _image.color = Color.black;
            }

            if (_hitPoints is AutoRecoveringHitPoints)
            {
                _image.color = Color.cyan;
            }
            
            if(_hitPoints == null) return;
            
            _hitPoints.OnHpChange += OnHpChange;
            UpdateSlider();
            UpdateText();
        }

        private void OnHpChange()
        {
            UpdateSlider();
            UpdateText();
        }

        private void UpdateSlider()
        {
            _slider.maxValue = _hitPoints.TotalHealth;
            _slider.value = _hitPoints.Health;
        }

        private void UpdateText()
        {
            if(_text == null) return;
            
            _text.text = $"{_hitPoints.Health}/{_hitPoints.TotalHealth}";
        }
        
        
        private void OnDestroy()
        {
            StopAllCoroutines();
        }
    }

}
