using System;
using Resources;
using TMPro;
using UnityEngine;

namespace UI
{
    public class UpdateMetalUI : MonoBehaviour
    {
        private TMP_Text _text;

        private void Awake()
        {
            _text = GetComponent<TMP_Text>();
        }

        private void GameResourcesOnOnMetalAmountChanged(object sender, EventArgs e)
        {
            _text.text = $"Metal - {ResourceManager.Instance.MetalAmount.ToString()}";
        }
    
        private void OnEnable()
        {
            ResourceManager.OnMetalAmountChanged += GameResourcesOnOnMetalAmountChanged;
        }

        private void OnDisable()
        {
            ResourceManager.OnMetalAmountChanged -= GameResourcesOnOnMetalAmountChanged;
        }
    }
}
