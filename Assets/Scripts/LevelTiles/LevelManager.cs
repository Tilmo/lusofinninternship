using Core;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace LevelTiles
{
    public class LevelManager : MonoBehaviour
    {
        public Tilemap wallMap;
        public Tilemap[] blockMaps;
        
        [SerializeField] private RuleTile wallTile;

        private void Start()
        {
            GameManager.Game.GridManager.LevelManager = this;
        }

        public bool HazardSpot(Vector3 position, Tilemap tilemap)
        {
            Vector3Int gridPosition = tilemap.WorldToCell( position );
 
            TileBase clickedTile = tilemap.GetTile(gridPosition);
            return clickedTile != null;
        }
        
        public void BuildWall(Vector3 position)
        {
            Vector3Int gridPosition = wallMap.WorldToCell( position );
            wallMap.SetTile(gridPosition, wallTile);
        }
        
        public void DestroyWall(Vector3 position)
        {
            Vector3Int gridPosition = wallMap.WorldToCell( position );
            wallMap.SetTile(gridPosition, null);
        }
    }
}
