﻿using Core;
using TDCamera;
using UnityEngine;

public class RaycastHitRaycast : IRaycastHit
{
    public Vector3 WorldPosition => GetPosition();
    public RaycastHit RaycastHit => GetHit();
    
    private readonly SurfaceCheckerTouch _surfaceChecker;

    public RaycastHitRaycast()
    {
        _surfaceChecker = new SurfaceCheckerTouch(new GameCamera());
    }
    public RaycastHitRaycast(LayerMask layerMask)
    {
        _surfaceChecker = new SurfaceCheckerTouch(new GameCamera(), layerMask);
    }
    private Vector3 GetPosition()
    {
        _surfaceChecker.Check();
        return _surfaceChecker.Found ? _surfaceChecker.hit.point : Vector3.zero;
    }

    private RaycastHit GetHit()
    {
        _surfaceChecker.Check();
        
        return _surfaceChecker.hit;
    }
}