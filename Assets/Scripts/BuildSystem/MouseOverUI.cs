﻿using UnityEngine.EventSystems;
public static class MouseOverUI
{
    public static bool HitUI()
    {
        return EventSystem.current.IsPointerOverGameObject();
    }

}