﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using LevelLoadSystem;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BuildingDataCollection : MonoBehaviour
{
    public BuildingData[] objects;

    public static BuildingDataCollection Instance => _instance;
    private static BuildingDataCollection _instance;
    private void Awake()
    {
        _instance = this;
        
        Initialize();
    }
    private void Initialize()
    {
        objects = UnityEngine.Resources.LoadAll<BuildingData>("Buildings");
            
        var go = gameObject;

        var scene = SceneManager.CreateScene(go.name);
        SceneManager.MoveGameObjectToScene(go,scene);
    }
}