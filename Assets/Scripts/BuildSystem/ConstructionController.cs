using System;
using System.Linq;
using UnityEngine;

public class ConstructionController : MonoBehaviour
{
    public event Action OnBuildComplete;
    public bool IsFinished => _hitPoints.Health >= _hitPoints.TotalHealth;
    
    [SerializeField] private MeshRenderer[] buildingModel;
    [SerializeField] private MeshRenderer underConstruction;
    
    private Core.Command _buildCommand;
    private Health.IDamageable _hitPoints;
    private int _ticksToComplete;
    private float Percentile => (100f /_ticksToComplete);

    public void Initialize(Health.IDamageable hitPoints, int ticks, Core.Command buildCommand )
    {
        if (buildingModel.Any())
        {
            foreach (var meshRenderer in buildingModel)
            {
                meshRenderer.enabled = false;
            }
        }

        _buildCommand = buildCommand;
        underConstruction.enabled = true;
        _ticksToComplete = ticks+1;

        _hitPoints = hitPoints;
    }

    public void CancelConstruction()
    {
        _buildCommand.Undo();
    }

    private void BuildingModel()
    {
        underConstruction.enabled = false;

        if (!buildingModel.Any()) return;
        
        foreach (var meshRenderer in buildingModel)
        {
            meshRenderer.enabled = true;
        }
    }

    
    public void FinishBuilding()
    {
        _hitPoints.HealDamage(_hitPoints.TotalHealth);
        
        BuildingModel();
        OnBuildComplete?.Invoke();
        
        ConstructionComplete();
    }

    public void ProgressBuilding()
    {
        _hitPoints.HealDamage( Mathf.CeilToInt((_hitPoints.TotalHealth * 0.01f) * Percentile));

        if (!IsFinished) return;
        FinishBuilding();
    }

    private void ConstructionComplete()
    {
        if (Core.GameManager.Game != null)
        {
            Core.GameManager.Game.GridManager.RefreshBuildable();
        }

    }

}
