﻿using UnityEngine;

public static class BuildingCollisionCheck
{
    private static readonly Collider[] TargetsBuffer = new Collider[1];
    private const int AlienLayerMask = 1 << 6;

    public static bool CheckCollision(Vector3 position)
    {
        var a = position;
        var b = a;
        b.y += 2f;

        var hits = Physics.OverlapCapsuleNonAlloc(a, b, Core.GameManager.Game.GridManager.GameGrid.Radius, TargetsBuffer,
            AlienLayerMask, QueryTriggerInteraction.Ignore);

        return hits > 0;
    }
}