﻿using Core;
using GridSystem.GridObjects;
using Humans;
using Resources;
using UnityEngine;

public class BuildWallCommand : Command
{
    private readonly Tile _tile;
    private readonly Wall _wall;
    private readonly BuildingData _data;

    private readonly ConstructionController _construction;

    public BuildWallCommand(Tile tile, BuildingData data)
    {
        _tile = tile;
        _data = data;

        _wall = _data.Building.GetComponent<Wall>();
        
        _tile.GridObject = _wall;
        
        _wall.Initialize(data);
        _wall.transform.position = tile.worldPos;

        _construction = _wall.GetComponentInChildren<ConstructionController>();
        _construction.Initialize(_wall.Health, data.buildTime, this);
        
        GameManager.Game.GridManager.AddToGridObjectList(_wall);
        
        ResourceManager.Instance.RemoveMetal(_data.metalCost);
        ResourceManager.Instance.UseHumans(_data.humanCost);
    }

    public override void Execute()
    {
        _construction.OnBuildComplete += ConstructionOnOnBuildComplete;
        ConstructionManager.Instance.AddToQueue(_construction);
    }

    private void ConstructionOnOnBuildComplete()
    {
        GameManager.Game.GridManager.LevelManager.BuildWall(_tile.worldPos);
        
        _construction.OnBuildComplete -= ConstructionOnOnBuildComplete;
    }

    public override void Undo()
    {
        _wall.Kill();
        ResourceManager.Instance.AddMetal(_data.metalCost);
        ResourceManager.Instance.FreeHumans(_data.humanCost);
    }
    
    public override bool IsFinished => _construction == null;
}