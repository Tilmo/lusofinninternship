using UnityEngine;

public class PreviewController : MonoBehaviour
{
    [SerializeField] private Material invalid;
    [SerializeField] private Material valid;
    
    private MeshRenderer _meshRenderer;

    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
    }
    public void Reset()
    {
        transform.parent.transform.position += Vector3.up*3;
    }
    public void UpdateMaterial(bool tileBuildable)
    {
        _meshRenderer.material = tileBuildable ? valid : invalid;
    }
}
