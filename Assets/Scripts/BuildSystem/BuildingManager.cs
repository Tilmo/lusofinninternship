using System.Collections.Generic;
using Core;
using GridSystem;
using UnityEngine;
using UnityEngine.UI;

public class BuildingManager : MonoBehaviour
{ 
    public static bool BuildMode { get; private set; }

    public Vector3 PreviewPosition => _previewPosition;
    public bool CanBuild(BuildingData data) => (BuildMode && TestBool(data));
    public bool CanBuild(BuildingData data, Vector2Int tile) => (BuildMode && TestBool(data, tile));

    [SerializeField] private Transform previewGameObject;
    [SerializeField] private Transform buildMenuParent;
    [SerializeField] private Button buttonTemplate;
    [SerializeField] private GameObject fakeCursor;

    private ButtonBuilder _buttonBuilder;
    private Vector3 _previewPosition;
    private PreviewController _previewController;
    private CommandProcessor _commandProcessor;
    private IRaycastHit _raycast;
    private MeshRenderer _previewMeshRenderer;
    private ConstructionManager _constructionManager;

    private bool TileIsBuildable => WorldPositionOnGrid && Core.GameManager.Game.GridManager.GameGrid.GetTile(_raycast.WorldPosition).Buildable;
    private bool TileIsEmpty => Core.GameManager.Game.GridManager.GameGrid.GetTile(_raycast.WorldPosition).Empty;
    private bool WorldPositionOnGrid => Core.GameManager.Game.GridManager.GameGrid.OnGrid(_raycast.WorldPosition);

    public void Initialize()
    {
        _previewController = GetComponentInChildren<PreviewController>();
        _previewMeshRenderer = previewGameObject.GetComponentInChildren<MeshRenderer>();
        _buttonBuilder = new GameObject("Button Builder").AddComponent<ButtonBuilder>();
        new GameObject("Build Data").AddComponent<BuildingDataCollection>();
        _buttonBuilder.Initialize(buttonTemplate, buildMenuParent, this);
        _constructionManager = GetComponent<ConstructionManager>();
    }

    public void StartGame(Vector3 pos)
    {
        _raycast = new FakeCursor(fakeCursor.transform);
        _commandProcessor = new CommandProcessor();

        _constructionManager.Initialize();
        
        BuildButtons();
        BuildBase(pos);
    }
    public void GameUpdate()
    {
        _commandProcessor.ProcessCommands();
        _constructionManager.GameUpdate();
        if (BuildMode)
        {
            BuildUpdate();
        }
    }
    
    private void BuildUpdate()
    {
        if (TileIsBuildable && TileIsEmpty) 
            _previewPosition = Core.GameManager.Game.GridManager.GameGrid.GridToWorldCentered(_raycast.WorldPosition);
        
        _previewController.UpdateMaterial(TileIsBuildable && TileIsEmpty );
        
        SetPreviewPosition(_previewPosition);
    }

    public static List<Vector2Int> GetBetween(int x1, int y1, int x2, int y2)
    {
        List<Vector2Int> list = new List<Vector2Int>();

        if (x1 == x2)
        {

            var smaller = y1 < y2 ? y1 : y2;
            for (int i = 0; i < Mathf.Abs(y1 - y2) +1; i++)
            {
                list.Add( new Vector2Int( x1, smaller + i ));
            }
        }
        
        else if (y1 == y2)
        {
            var smaller = x1 < x2 ? x1 : x2;
            for (int i = 0; i < Mathf.Abs(x1 - x2) +1; i++)
            {
                list.Add( new Vector2Int( smaller + i, y1 ));
            }
        }
        
        return list;
    }

    private void BuildButtons()
    {
        _buttonBuilder.BuildButtons();
    }
    public void DestroyButtons()
    {
        _buttonBuilder.DestroyButtons();
    }
    private void BuildModeToggle()
    {
        BuildMode = !BuildMode;
        
        _previewMeshRenderer.enabled = BuildMode;
    }

    public void ActivateBuildMode()
    {
        if(BuildMode)
            Core.GameManager.Game.GridManager.HideGrid();
        else
        {
            Core.GameManager.Game.GridManager.UpdateGridVisual();
        }
 
        BuildModeToggle();
    }
    public void Build(BuildingData data)
    {
        if (Resources.ResourceManager.Instance.MetalAmount < data.metalCost ||
            (Resources.ResourceManager.Instance.HumanAmount) < data.humanCost)
        {
            return;
        }
        
        var tile = GameManager.Game.GridManager.GameGrid.GetTile(_previewPosition);
        
        _commandProcessor.AddCommand( data.BuildCommand(tile) );
    }

    public void Build(BuildingData data, Vector2Int pos)
    {
        var tile = GameManager.Game.GridManager.GameGrid.GetTile(pos);
        
        _commandProcessor.AddCommand( data.BuildCommand(tile) );
    }
    
    
    private bool TestBool(BuildingData data)
    {
        if(!Core.GameManager.Game.GridManager.GameGrid.GetTile(_previewPosition).Empty) return false;
        if (!WorldPositionOnGrid || Core.GameManager.Game.GridManager.GameGrid.GridToWorldCentered(_raycast.WorldPosition) != _previewPosition
                                 || BuildingCollisionCheck.CheckCollision(_previewPosition)) return false;
        
        if (Resources.ResourceManager.Instance.MetalAmount < data.metalCost ||
            (Resources.ResourceManager.Instance.HumanAmount) < data.humanCost)
        {
            return false;
        }

        return true;
    }
    
    private bool TestBool(BuildingData data, Vector2Int pos)
    {
        var tile = GameManager.Game.GridManager.GameGrid.GetTile(pos);

        if (tile == null) return false;
        if(!GameManager.Game.GridManager.GameGrid.GetTile( pos ).Empty) return false;
        if(BuildingCollisionCheck.CheckCollision(_previewPosition)) return false;
        
        if (Resources.ResourceManager.Instance.MetalAmount < data.metalCost ||
            (Resources.ResourceManager.Instance.HumanAmount) < data.humanCost)
        {
            return false;
        }

        return true;
    }
    
    private void SetPreviewPosition(Vector3 pos)
    {
        if (!WorldPositionOnGrid) return;
        previewGameObject.position = Core.GameManager.Game.GridManager.GameGrid.GridToWorldCentered(_raycast.WorldPosition) == _previewPosition ? pos : _raycast.WorldPosition;
    }

    [SerializeField] private GameObject playerBasePrefab;

    private void BuildBase(Vector3 startingPosition)
    {
        GridManager gridManager = Core.GameManager.Game.GridManager;
        
        var tile = gridManager.GameGrid.GetTile(startingPosition);
        var buildBase = Instantiate(playerBasePrefab).GetComponent<Humans.Base>();
        
        tile.GridObject = buildBase;
        buildBase.Tile = tile;

        buildBase.transform.position = tile.worldPos;

        buildBase.Initialize();

        var list = buildBase.Tile.GetNeighbours(1, 1);
            
        foreach (var t in list)
        {
            t.GridObject = buildBase;
        }
        
        gridManager.RefreshBuildable();
        gridManager.AddToGridObjectList(buildBase);
    }
}