﻿using UnityEngine;

public class FakeCursor : IRaycastHit
{
    public RaycastHit RaycastHit => GetHit();
    public Vector3 WorldPosition => _transform.position;
    
    private readonly Transform _transform;
    private RaycastHit _hit;
    public FakeCursor(Transform transform)
    {
        _transform = transform;
    }
    
    private RaycastHit GetHit()
    {
        if (Physics.Raycast(WorldPosition, Vector3.up, out _hit, 100f)) 
            return _hit;

        return _hit;
    }
}