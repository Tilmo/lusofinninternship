﻿using Core;
using GridSystem.GridObjects;
using Humans;
using Resources;

public class BuildTurretCommand : Command
{
    private readonly Turret _turret;
    private readonly BuildingData _data;

    private readonly ConstructionController _construction;

    public BuildTurretCommand(Tile tile, BuildingData data)
    {
        _data = data;

        _turret = _data.Building.GetComponent<Turret>();
        tile.GridObject = _turret;
        
        _turret.Initialize(data);
        _turret.transform.position = tile.worldPos;
        
        _construction = _turret.GetComponentInChildren<ConstructionController>();
        _construction.Initialize(_turret.Health, data.buildTime, this);

        GameManager.Game.GridManager.AddToGridObjectList(_turret);
        
        ResourceManager.Instance.RemoveMetal(_data.metalCost);
        ResourceManager.Instance.UseHumans(_data.humanCost);
    }

    public override void Execute()
    {
        _construction.OnBuildComplete += ConstructionOnOnBuildComplete;
        ConstructionManager.Instance.AddToQueue(_construction);
    }

    private void ConstructionOnOnBuildComplete()
    {
        _turret.Arm();
        _construction.OnBuildComplete -= ConstructionOnOnBuildComplete;
    }
    
    public override void Undo()
    {
        _turret.Kill();
        ResourceManager.Instance.AddMetal(_data.metalCost);
        ResourceManager.Instance.FreeHumans(_data.humanCost);
    }
    
    public override bool IsFinished => _construction == null;
}