﻿using Core;
using GridSystem.GridObjects;
using Health;
using Humans;
using UnityEngine;
using Weapons;

[CreateAssetMenu(menuName = "Create BuildingCommandObject", fileName = "BuildingCommandObject", order = 0)]
public class BuildingData : ScriptableObject
{
    public enum Type { None, Wall, Pylon, Turret }
    public enum Weapon { None, Ballistic, Tesla, Flame, Coil }

    public string buildingName;
    public GameObject prefab;
    public Sprite iconUI;

    public Type buildingType;

    public int metalCost;
    public int humanCost;
    public int buildRange;
    public int buildTime;

    public int startingHealth;
    public int maxHealth;

    public Weapon weapon;
    public DamageType damageType;
    
    public float attackRange;
    public float attackSpeed;
    public float attackDamage;
    public float moveSpeed;

    public GameObject Building => BuildBuilding();
    private GameObject BuildBuilding()
    {
        var go = Instantiate(prefab);
        
        AddWeapon(go);
        
        var a = go.GetComponent<GridObject>();
        a.Initialize();

        var hp = go.GetComponent<IDamageable>();
        hp.TotalHealth = maxHealth;
        hp.Health = startingHealth;
        
        go.name = buildingName;
        
        return go;
    }
    
    public void BuildTrigger()
    {
        if(GameManager.Game.BuildingManager.CanBuild(this))
        {
            GameManager.Game.BuildingManager.Build(this);
        }
    }

    private Vector3 _startPosition;
    private Vector3 _endPosition;

    public void BuildStart(Vector3 startPos)
    {
        _startPosition = startPos;
    }

    public void BuildEnd(Vector3 endPos)
    {
        _endPosition = endPos;

        foreach (Vector2Int v2 in BuildingManager.GetBetween((int) _startPosition.x, (int) _startPosition.z,
            (int) _endPosition.x, (int) _endPosition.z))
        {
            if(GameManager.Game.BuildingManager.CanBuild(this, v2))
            {
                GameManager.Game.BuildingManager.Build(this, v2);
            }
        }
    }

    private void AddWeapon(GameObject go)
    {
        var weaponType = weapon;

        if (weaponType == Weapon.Ballistic)
        {
            var wp = go.AddComponent<BallisticWeapon>();
            wp.SetDamageType( damageType );
        }
        
        if (weaponType == Weapon.Tesla)
        {
            var wp = go.AddComponent<TeslaWeapon>();
            wp.SetDamageType( damageType );
        }

        if (weaponType == Weapon.Flame)
        {
            var wp = go.AddComponent<FlameThrowerWeapon>();
            wp.SetDamageType( damageType );
        }
        
        if (weaponType == Weapon.Coil)
        {
            var wp = go.AddComponent<CoilWeapon>();
            wp.SetDamageType( damageType );
        }
    }
    public Command BuildCommand(Tile tile) => GetBuildCommand(tile);
    private Command GetBuildCommand(Tile tile)
    {
        if(buildingType == Type.Pylon)
            return new BuildPylonCommand(tile, this);
        
        if(buildingType == Type.Wall)
            return new BuildWallCommand(tile, this);

        if (buildingType == Type.Turret)
            return new BuildTurretCommand(tile, this);

        
        
        Debug.Log("Type Error");
        return null;
    }
    
}
