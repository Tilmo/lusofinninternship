﻿using UnityEngine;
public interface IRaycastHit
{
    Vector3 WorldPosition { get; }
    RaycastHit RaycastHit { get; }
}