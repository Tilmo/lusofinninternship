﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core;
using UnityEngine;

public class ConstructionManager : MonoBehaviour
{
    public static ConstructionManager Instance => _instance;
    private static ConstructionManager _instance;

    private Queue<ConstructionController> _constructionQueue;
    private ConstructionController _current;
    private bool _combatStarted;

    private void Awake()
    {
        _instance = this;
        _constructionQueue = new Queue<ConstructionController>();
    }

    public void Initialize()
    {
        _combatStarted = false;
        TimeTickManager.OnTimeTick -= TimeTickManagerOnOnTimeTick;
        TimeTickManager.OnTimeTick += TimeTickManagerOnOnTimeTick;
    }

    private void TimeTickManagerOnOnTimeTick(object sender, EventArgs e)
    {
        if (!_combatStarted) _combatStarted = true;
        
        if(_current==null) return;
        
        _current.ProgressBuilding();
    }

    public void GameUpdate()
    {
        if(_current == null)
            SetNext();
    }

    public void AddToQueue(ConstructionController controller)
    {
        _constructionQueue.Enqueue(controller);
    }

    private void SetNext()
    {
        if (_constructionQueue.Count > 0)
        {
            _current = _constructionQueue.Dequeue();

            if (_combatStarted)
            {
                if (_current.IsFinished)
                {
                    _current.FinishBuilding();
                    _current = null;
                }
                else
                {
                    _current.OnBuildComplete += CurrentOnOnBuildComplete; 
                }
            }

            else
            {
                _current.FinishBuilding();
                _current = null;
            }
            

        }
    }
    
    private void CurrentOnOnBuildComplete()
    {
        _current.OnBuildComplete -= CurrentOnOnBuildComplete;
        _current = null;
    }
    
}