﻿using Core;
using GridSystem.GridObjects;
using Humans;
using Resources;
public class BuildPylonCommand : Command
{
    private readonly Pylon _pylon;

    private readonly BuildingData _data;
    private readonly ConstructionController _construction;

    public BuildPylonCommand(Tile tile, BuildingData data)
    {
        var tile1 = tile;
        _data = data;
        _pylon = _data.Building.GetComponent<Pylon>();

        _construction = _pylon.GetComponentInChildren<ConstructionController>();
        tile1.GridObject = _pylon;
        _pylon.Initialize(tile1, _data);

        _construction.Initialize(_pylon.Health, data.buildTime, this);
        
        GameManager.Game.GridManager.AddToGridObjectList(_pylon);
        
        ResourceManager.Instance.RemoveMetal( _data.metalCost );
        ResourceManager.Instance.UseHumans( _data.humanCost );
    }


    public override void Execute()
    {
        _construction.OnBuildComplete += ConstructionOnOnBuildComplete;
        ConstructionManager.Instance.AddToQueue(_construction);
    }
    
    private void ConstructionOnOnBuildComplete()
    {
        _pylon.Arm();
        _construction.OnBuildComplete -= ConstructionOnOnBuildComplete;
    }
        
    public override void Undo()
    {
        ResourceManager.Instance.AddMetal( _data.metalCost);
        ResourceManager.Instance.FreeHumans( _data.humanCost);
            
        _pylon.Kill();
    }
    public override bool IsFinished => _construction == null;
}