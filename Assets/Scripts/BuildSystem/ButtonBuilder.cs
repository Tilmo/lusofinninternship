﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonBuilder : MonoBehaviour
{
    [SerializeField] private Transform buttonParentTransform;
    [SerializeField] private Button prefab;

    
    private List<Button> _buttons;
    private BuildingManager _buildingManager;

    public void Initialize(Button buttonPrefab, Transform goTransform, BuildingManager buildingManager)
    {
        _buildingManager = buildingManager;
        
        this.prefab = buttonPrefab;
        buttonParentTransform = goTransform;
        
        _buttons = new List<Button>();
    }
    public void BuildButtons()
    {
        _buttons = new List<Button>();
        var array = BuildingDataCollection.Instance.objects;

        foreach (var t in array)
        {
            var button = Instantiate(prefab, buttonParentTransform, false);
            var sprite = t.iconUI;
            
            if(sprite != null) 
                button.GetComponent<Image>().sprite = t.iconUI;
            
            EventTrigger eventTrigger = button.gameObject.AddComponent<EventTrigger>();

            var pointerDown = new EventTrigger.Entry {eventID = EventTriggerType.PointerDown};
            pointerDown.callback.AddListener((e) => t.BuildStart( _buildingManager.PreviewPosition ));

            var pointerUp = new EventTrigger.Entry {eventID = EventTriggerType.PointerUp};
            pointerUp.callback.AddListener((e) => t.BuildEnd( _buildingManager.PreviewPosition ));
                
                
            eventTrigger.triggers.Add(pointerUp);
            eventTrigger.triggers.Add(pointerDown);

            _buttons.Add(button);
        }
    }
    public void DestroyButtons()
    {
        foreach (var button in _buttons)
        {
            Destroy(button.gameObject);
        }
    }
}