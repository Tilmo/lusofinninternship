using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class VFXController : MonoBehaviour
{
    [SerializeField]
    private VisualEffect visualEffect;
    [SerializeField]
    private float effectDuration = 1;
    private Transform rayTarget;
    
    [SerializeField]
    private Vector3 rayAngle;
    private Vector3 vfxPos;

    private RaycastHit[] hits;

    void Awake()
    {
        if (visualEffect == null) //find the correct VFX from prefab
        {
            var i = transform.Find("AttackVFX");
            if (i != null)
            {
                visualEffect = i.GetComponent<VisualEffect>();
            }
            else
            {
                var j = transform.Find("Turret").Find("AttackVFX");
                if (j != null)
                {
                    visualEffect = j.GetComponent<VisualEffect>();
                }
                else
                {
                    Debug.Log("Looking for VFX in all children. To ensure the locating of the correct VFX make sure to place the attack VFX inside the object AttackVFX");
                    visualEffect = GetComponentInChildren<VisualEffect>();
                    if (visualEffect == null)
                        Debug.LogError("Could not find the VFX from game object.");
                }
            }
        }
    }

    private void Update()
    {
        Debug.DrawRay(vfxPos, rayAngle, Color.red);
    }

    public void enableVFX(Transform enemyTransform, int i, int l) //inherit enemy position from turret
    {
        vfxPos = visualEffect.transform.position;
        rayTarget = enemyTransform.GetComponentInChildren<RayTargetIndicator>().transform;

        if (rayTarget == null)
        {
            Debug.Log("Could not find ray target. Setting to default.");
            rayTarget = enemyTransform;
        }
        else
        {
            Debug.Log("Target found at: " + rayTarget);
        }

        var ePos = rayTarget.position; //The position of the target
        var enemyAngle = Quaternion.LookRotation(ePos - vfxPos, Vector3.up).eulerAngles; //angle to rotate bullet trails/lightning etc. facing the enemy
        rayAngle = ePos - vfxPos;

        Ray ray = new Ray (vfxPos, rayAngle);
        RaycastHit[] hits = Physics.RaycastAll(ray, Vector3.Distance(vfxPos, ePos));

        foreach (RaycastHit hit in hits)
        {
            Debug.Log("Ray hit something");
            if (hit.transform == enemyTransform)
            {
                Debug.Log("Enemy hit");
                ePos = hit.point;
            }
            else if (hit.transform.root == enemyTransform)
            {
                Debug.Log("Enemy hit");
                ePos = hit.point;
            }
        }

        if (i > 0)
        {
            StartCoroutine(DelayedVFX(i, l, ePos, enemyAngle));

            if (effectDuration > 0)
            {
                StartCoroutine(disableVFX(effectDuration));
            }
        }
        else
        {
            visualEffect.SetVector3("enemyPosition", ePos);
            visualEffect.SetVector3("enemyAngle", enemyAngle);
            //Debug.Log("Starting vfx towards target: " + i);
            visualEffect.SendEvent("StartVFX");
            if (effectDuration > 0)
            {
                StartCoroutine(disableVFX(effectDuration));
            }
        }
        //set parametres for the VFX and start it

    }

    private IEnumerator disableVFX(float t)
    {
        yield return new WaitForSeconds(t);
        visualEffect.SendEvent("EndVFX");
    }

    //This is a workaround to wait for frames, since VFX graph can't call an event more than once per frame
    public IEnumerator DelayedVFX(int i, int l, Vector3 ePos, Vector3 enemyAngle) 
    {
        yield return new WaitForSeconds(1f / l * i);

        visualEffect.SetVector3("enemyPosition", ePos);
        visualEffect.SetVector3("enemyAngle", enemyAngle);
        //Debug.Log("Starting vfx towards target: " + i);
        visualEffect.SendEvent("StartVFX");
    }
}

