using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXTestDummy : MonoBehaviour
{
    [SerializeField]
    private VFXController VFXController;
    [SerializeField]
    private Dummy[] dummies;

    private void Awake()
    {
        VFXController = FindObjectOfType<VFXController>();
    }

    public void testVFX()
    {
        VFXController = FindObjectOfType<VFXController>();
        dummies = FindObjectsOfType<Dummy>();

        var i = 0;
        while (i < dummies.Length)
        {
            Debug.Log("Dummy testing vfx");
            VFXController.enableVFX(dummies[i].transform, i, dummies.Length);

            i++;
        }
    }

}
