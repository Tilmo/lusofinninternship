﻿using Core;
using Health;
using StatSystem;
using UnityEngine;

namespace Weapons
{
    public interface IWeapon
    {
        public DamageType DamageType { get; }
        public void Fire();

        public ITarget TargetFinder { get; }
        
        public Transform FirePoint { get; set; }
    }
}