﻿using Enemies.Targeting;
using StatSystem;
using UnityEngine;

namespace Weapons
{
    public class OverlapCapsuleTargetFinder : MonoBehaviour, ITarget
    {
        public TargetPoint Target
        {
            get => TargetUpdate();
            set => _target = value;
        }
        
        private TargetPoint _target;
        private Stats _stats;
        
        private static readonly Collider[] TargetsBuffer = new Collider[1];
        private const int AlienLayerMask = 1 << 6;

        private void Awake()
        {
            _stats = GetComponent<Stats>();
        }

        private TargetPoint TargetUpdate()
        {
            if (_target == null)
            {
                AcquireTarget();
            }
            else
            {
                TrackTarget();
            }
            return _target;
        }

        private void TrackTarget()
        {
            if (_target == null) return;

            var a = transform.localPosition;
            var b = _target.Position;

            var x = a.x - b.x;
            var z = a.z - b.z;
            
            var r = _stats.AttackRange + 2f;

            if (x * x + z* z > r * r)
            {
                _target = null;
            }
        }

        private void AcquireTarget()
        {
            Vector3 a = transform.localPosition;
            Vector3 b = a;
            b.y += 2f;
            float r = _stats.AttackRange;
            
            int hits = Physics.OverlapCapsuleNonAlloc(a, b, r, TargetsBuffer, AlienLayerMask);

            if (hits > 0)
            {
                _target = TargetsBuffer[0].GetComponent<TargetPoint>();
                Debug.Assert(_target !=null, "Targeted non-enemy", TargetsBuffer[0]);
            }
            else
            {
                _target = null;
            }
        }
        
        public static int GetTargets(int layerMask, Vector3 position, Collider[] targetBuffer)
        {
            Vector3 a = position;
            Vector3 b = a;
            b.y += 2f;

            int hits = Physics.OverlapCapsuleNonAlloc(a, b,1f, targetBuffer, layerMask);

            return hits;
        }
        
        
    }
}