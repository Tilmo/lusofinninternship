﻿using Enemies.Targeting;

namespace Weapons
{
    public interface ITarget
    {
        public TargetPoint Target { get; set; }
    }

}