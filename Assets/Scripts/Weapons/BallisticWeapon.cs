﻿using Core;
using StatSystem;
using UnityEngine;

namespace Weapons
{
    public class BallisticWeapon : MonoBehaviour, IWeapon
    {
        public DamageType DamageType => _damageType;
        public ITarget TargetFinder => _target;
        public Transform FirePoint { get; set; }
        
        private DamageType _damageType;
        private ITarget _target;
        private IHaveStats _stats;

        [SerializeField]
        private VFXController VFXController;

        private void Awake()
        {
            _target = gameObject.AddComponent<OverlapCapsuleTargetFinder>();
            _stats = GetComponent<IHaveStats>();

            VFXController = transform.GetComponent<VFXController>();
        }
        public void SetDamageType(DamageType damageType)
        {
            _damageType = damageType;
        }
        
        public void Fire()
        {
            if (TargetFinder?.Target)
            {
                if(_target.Target == null) return;

                _target.Target.Alien.Damageable.TakeDamage( Mathf.FloorToInt( _stats.AttackDamage ), DamageType);

                VFXController.enableVFX(_target.Target.transform, 0, 1);
            }
        }
    }
}