﻿using System;
using Core;
using Health;
using StatSystem;
using UnityEngine;

namespace Weapons
{
    public class TeslaWeapon : MonoBehaviour, IWeapon
    {
        public ITarget TargetFinder => _target;
        public DamageType DamageType => _damageType;
        public Transform FirePoint { get; set; }

        private IHaveStats _stats;
        private ITarget _target;
        private Transform _transform;
        private const int AlienLayerMask = 1 << 6;
        private DamageType _damageType = DamageType.Normal;

        private VFXController VFXController;

        private void Awake()
        {
            _target = gameObject.AddComponent<OverlapCapsuleTargetFinder>();
            _stats = GetComponent<IHaveStats>();
            _transform = transform;

            VFXController = transform.GetComponent<VFXController>();
            Debug.Log(transform);
        }
        public void SetDamageType(DamageType damageType)
        {
            _damageType = damageType;
        }
        
        public void Fire()
        {
            var j = GetTargets(AlienLayerMask, _transform.position);

            for (int i = 0; i < j; i++)
            {
                var a = _targetBuffer[i].transform.root.GetComponent<IDamageable>();
                Debug.Log("Calling vfx to: " + _targetBuffer[i].transform + "     Target i: " +i);
                VFXController.enableVFX(_targetBuffer[i].transform, i, _targetBuffer.Length);

                a.TakeDamage((int)_stats.AttackDamage);
                Debug.Log("Hit Something");
                
                var b = _targetBuffer[i].transform.root.GetComponent<IHaveStats>();
                b.MultiplyStat(Stat.MoveSpeed, -25f, 1.5f);
            }
        }
        
        private readonly Collider[] _targetBuffer = new Collider[10];
        
        private int GetTargets(int layerMask, Vector3 position)
        {
            Vector3 a = position;
            Vector3 b = a;
            b.y += 2f;

            int hits = Physics.OverlapCapsuleNonAlloc(a, b, _stats.AttackRange, _targetBuffer, layerMask);

            return hits;
        }
    }
}