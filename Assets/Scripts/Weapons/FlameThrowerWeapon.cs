﻿using System;
using System.Collections.Generic;
using Core;
using Health;
using StatSystem;
using UnityEngine;

namespace Weapons
{
    public class FlameThrowerWeapon : MonoBehaviour, IWeapon
    {
        public ITarget TargetFinder => _target;
        public Transform FirePoint { get; set; }
        public DamageType DamageType => _damageType;

        private IHaveStats _stats;
        private ITarget _target;
        private Transform _transform;
        private DamageType _damageType = DamageType.Normal;
        
        private readonly Collider[] _targetBuffer = new Collider[10];
        private const int AlienLayerMask = 1 << 6;
        
        private void Awake()
        {
            _stats = GetComponent<IHaveStats>();
            _target = gameObject.AddComponent<OverlapCapsuleTargetFinder>();
      
            _transform = transform;
        }

        public void SetDamageType(DamageType damageType)
        {
            _damageType = damageType;
        }
        
        
        public void Fire()
        {
            var a = GetHits();
            
            foreach (Collider target in a)
            {
                target.transform.root.GetComponent<IDamageable>().TakeDamage( (int)_stats.AttackDamage);
            }
        }

        private List<Collider> GetHits()
        {
            List<Collider> list = new List<Collider>();

            for (int i = 0; i < (int) _stats.AttackRange; i++)
            {
                var hits = GetTargets(AlienLayerMask, _transform.position + (FirePoint.forward * i));
                
                for (int j = 0; j < hits; j++)
                {
                    list.Add(_targetBuffer[j]);
                }
            }
            return list;
        }
        
        private int GetTargets(int layerMask, Vector3 position)
        {
            Vector3 a = position;
            Vector3 b = a;
            b.y += 2f;

            int hits = Physics.OverlapCapsuleNonAlloc(a, b,1f, _targetBuffer, layerMask);

            return hits;
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            for (int i = 0; i < (int) _stats.AttackRange; i++)
            {
                Gizmos.DrawWireSphere(  _transform.position + (FirePoint.forward * i)  , 1f);
            }
        }
#endif
    }
    
}
