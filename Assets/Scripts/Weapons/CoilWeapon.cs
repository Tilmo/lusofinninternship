﻿using System;
using Core;
using Health;
using StatSystem;
using UnityEngine;

namespace Weapons
{
    public class CoilWeapon : MonoBehaviour, IWeapon
    {
        public DamageType DamageType => _damageType;
        private DamageType _damageType;

        private IHaveStats _stats;
        private OverlapCapsuleTargetFinder _target;
        private Transform _transform;
        private void Awake()
        {
            _stats = GetComponent<IHaveStats>();
            _target = gameObject.AddComponent<OverlapCapsuleTargetFinder>();
      
            _transform = transform;
        }
        public void SetDamageType(DamageType damageType)
        {
            _damageType = damageType;
        }
        
        public void Fire()
        {
            if (TargetFinder?.Target)
            {
                if(_target.Target == null) return;
                
                _target.Target.Alien.Damageable.TakeDamage( Mathf.FloorToInt( _stats.AttackDamage ), DamageType);

                for (int i = 0;
                    i < OverlapCapsuleTargetFinder.GetTargets(AlienLayerMask, _target.transform.position,
                        _targetBuffer);
                    i++)
                {
                    _targetBuffer[i].transform.root.GetComponent<IDamageable>().TakeDamage((int)(_stats.AttackDamage * .7f));
                }
                
            }
        }

        private const int AlienLayerMask = 1 << 6;
        private readonly Collider[] _targetBuffer = new Collider[5];
        
        public ITarget TargetFinder => _target;
        public Transform FirePoint { get; set; }
    }
}