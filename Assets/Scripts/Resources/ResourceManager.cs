﻿using System;
using Serialization;
using UnityEngine;

namespace Resources
{
    public class ResourceManager : MonoBehaviour
    {
        public static ResourceManager Instance => _instance;
        private static ResourceManager _instance;
        public ResourceWorldSession ResourceWorldSession => resourceWorldSession;

        public static event EventHandler OnMetalAmountChanged;
        public static event EventHandler OnResearchAmountChanged;
        public static event EventHandler OnHumansAmountChanged;
        
        public static event EventHandler OnMetalAmountChangedWorld;
        public static event EventHandler OnResearchAmountChangedWorld;
        public static event EventHandler OnHumansAmountChangedWorld;
        
        public ResourceWorldSession resourceWorldSession;
        private ResourceSession _resourceSession;

        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
            } else {
                _instance = this;
            }
        }

        public void Initialize()
        {
            StartNewWorldSession();
            InitializeWorldMapUI();
        }

        [ContextMenu("Start New Session")]
        public void StartGame()
        {
            Debug.Assert( resourceWorldSession != null, "Save Game Error");
            
            StartNewSession(resourceWorldSession);
        }

        public void StartNewWorldSession()
        {
            int startingMetal = SaveData.Current.metalAmount;
            int humans = SaveData.Current.humansAmount;
            int research = SaveData.Current.researchAmount;
            
            resourceWorldSession = new ResourceWorldSession(research, startingMetal, humans);
            
            InitializeWorldMapUI();
        }
        
        public int HumanAmount => _resourceSession.HumanAmount;
        public int HumansUsed => _resourceSession.HumansUsed;
        public int ResearchAmount => _resourceSession.ResearchAmount;
        public int MetalAmount => _resourceSession.MetalAmount;

        public void AddMetal(int amount)
        {
            _resourceSession.MetalAmount += amount;
            OnMetalAmountChanged?.Invoke(null, EventArgs.Empty);
        }

        public void RemoveMetal(int amount)
        {
            _resourceSession.MetalAmount -= amount;
            OnMetalAmountChanged?.Invoke(null, EventArgs.Empty);
        }

        public void AddResearch(int amount)
        {
            _resourceSession.ResearchAmount += amount;
            OnResearchAmountChanged?.Invoke(null, EventArgs.Empty);
        }

        public void AddHumans(int amount)
        {
            _resourceSession.HumanAmount += amount;
            OnHumansAmountChanged?.Invoke(null, EventArgs.Empty);
        }

        public void RemoveHumans(int amount)
        {
            _resourceSession.HumanAmount -= amount;
            OnHumansAmountChanged?.Invoke(null, EventArgs.Empty);
        }

        public void UseHumans(int amount)
        {
            _resourceSession.HumansUsed += amount;
            _resourceSession.HumanAmount -= amount;
            
            OnHumansAmountChanged?.Invoke(null, EventArgs.Empty);
        }

        public void FreeHumans(int amount)
        {
            _resourceSession.HumansUsed -= amount;
            _resourceSession.HumanAmount += amount;
            OnHumansAmountChanged?.Invoke(name, EventArgs.Empty);
        }
        
        
        public void AddMetalWorld(int amount)
        {
            resourceWorldSession.StartingMetal += amount;
            OnMetalAmountChangedWorld?.Invoke(null, EventArgs.Empty);
        }

        public void RemoveMetalWorld(int amount)
        {
            resourceWorldSession.StartingMetal -= amount;
            OnMetalAmountChangedWorld?.Invoke(null, EventArgs.Empty);
        }

        public void AddResearchWorld(int amount)
        {
            resourceWorldSession.ResearchAmount += amount;
            OnResearchAmountChangedWorld?.Invoke(null, EventArgs.Empty);
        }
        public void RemoveResearchWorld(int amount)
        {
            resourceWorldSession.ResearchAmount -= amount;
            OnResearchAmountChangedWorld?.Invoke(null, EventArgs.Empty);
        }

        public void AddHumansWorld(int amount)
        {
            resourceWorldSession.HumanAmount += amount;
            OnHumansAmountChangedWorld?.Invoke(null, EventArgs.Empty);
        }
        public void RemoveHumansWorld(int amount)
        {
            resourceWorldSession.HumanAmount -= amount;
            OnHumansAmountChangedWorld?.Invoke(null, EventArgs.Empty);
        }
        

        [ContextMenu("End Session")]
        public void EndSession()
        {
            resourceWorldSession.HumanAmount = _resourceSession.HumanAmount + _resourceSession.HumansUsed;
            resourceWorldSession.ResearchAmount += _resourceSession.ResearchAmount;
            
            OnHumansAmountChangedWorld?.Invoke(null, EventArgs.Empty);
            OnResearchAmountChangedWorld?.Invoke(null, EventArgs.Empty);
        }

        private void StartNewSession(ResourceWorldSession data)
        {
            int startingMetal = data.StartingMetal;
            int humans = data.HumanAmount;
            
            _resourceSession = new ResourceSession(startingMetal, humans);
        }


        [ContextMenu("Init World map UI")]
        public void InitializeWorldMapUI()
        {
            OnMetalAmountChangedWorld?.Invoke(null, EventArgs.Empty);
            OnHumansAmountChangedWorld?.Invoke(name, EventArgs.Empty);
            OnResearchAmountChangedWorld?.Invoke(null, EventArgs.Empty);
        }
        public void InitializeUI()
        {
            OnMetalAmountChanged?.Invoke(null, EventArgs.Empty);
            OnHumansAmountChanged?.Invoke(name, EventArgs.Empty);
            OnResearchAmountChanged?.Invoke(null, EventArgs.Empty);
        }

        
    }
}