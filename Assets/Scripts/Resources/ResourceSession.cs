﻿namespace Resources
{
    public class ResourceSession
    {
        public ResourceSession(int metalAmount, int humanAmount)
        {
            MetalAmount = metalAmount;
            HumanAmount = humanAmount;
        }
        
        public int MetalAmount { get; set; }

        public int HumanAmount { get; set; }

        public int ResearchAmount { get; set; }

        public int HumansUsed { get; set; }
    }
}