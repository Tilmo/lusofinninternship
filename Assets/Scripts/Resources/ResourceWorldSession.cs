﻿namespace Resources
{
    public class ResourceWorldSession
    {
        public int StartingMetal { get; set; }

        public int HumanAmount { get; set; }

        public int ResearchAmount { get; set; }

        public ResourceWorldSession(int researchAmount, int startingMetal, int humanAmount)
        {
            ResearchAmount = researchAmount;
            StartingMetal = startingMetal;
            HumanAmount = humanAmount;
        }
    }
}