﻿using System;
using Core;
using Resources;
using UnityEngine;

namespace GridSystem
{
    public class MiningNodeLevelGridObject : MonoBehaviour
    {
        private void OnEnable()
        {
            TimeTickManager.OnTimeTick += TimeTickManagerOnOnTimeTick;
        }

        private void OnDisable()
        {
            TimeTickManager.OnTimeTick -= TimeTickManagerOnOnTimeTick;
        }

        private void TimeTickManagerOnOnTimeTick(object sender, EventArgs e)
        {
            if (GameManager.Game.GridManager.GameGrid.GetTile(transform.position).Buildable)
            {
                ResourceManager.Instance.AddMetal(10);
                ResourceManager.Instance.AddResearch(1);
            }
            
        }
    }
}