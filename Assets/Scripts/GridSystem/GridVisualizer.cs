using Core;
using LevelLoadSystem;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GridSystem
{
    
    public class GridVisualizer : MonoBehaviour
    {
        [SerializeField] private GameObject floorQuadPrefab;
        [SerializeField] private LevelLoader levelLoader;
        
        private readonly MeshRenderer[,] _buildTiles = new MeshRenderer[35, 35];
        private bool _initialized;
        private Transform _proxy;

        public void HideGridTiles()
        {
            var grid = GameManager.Game.GridManager.GameGrid;
            if(grid == null) return;
            
            for (int i = 0; i < grid.dimensions.x; i++)
            {
                for (int j = 0; j < grid.dimensions.y; j++)
                {
                    _buildTiles[i, j].enabled = false;
                }
            }
        }
        public void BuildFullGrid()
        {
            var gameGrid = GameManager.Game.GridManager.GameGrid;
            
            _proxy = new GameObject("GridVisProxy").transform;
            
            if (!_initialized)
            {
                levelLoader.LoadScene( SceneManager.CreateScene("Grid Visualizer"));
                SceneManager.MoveGameObjectToScene( _proxy.gameObject, SceneManager.GetSceneByName("Grid Visualizer"));
                _initialized = true;
            }
            
            for (int i = 0; i < 35; i++)
            {
                for (int j = 0; j < 35; j++)
                {
                    if(_buildTiles[i,j] != null) return;
                    
                    var go = Instantiate(floorQuadPrefab, _proxy);
                    var r = gameGrid.diameter * .9f;
                    go.transform.localScale = new Vector3(r, r, r);
                    go.transform.position = gameGrid.GridToWorldCentered(i, 0, j);
                    go.transform.parent = null;
                    
                    _buildTiles[i, j] = go.GetComponent<MeshRenderer>();
                }
            }  
            
        }
        public void UpdateGrid()
         {
             var grid = GameManager.Game.GridManager.GameGrid;
             if(grid == null) return;
             
             for (int i = 0; i < grid.dimensions.x; i++)
             {
                 for (int j = 0; j < grid.dimensions.y; j++)
                 {
                     var gridPiece = _buildTiles[i, j];
                     var tile = grid.Tiles[i, j];

                     if (!gridPiece.enabled)
                     {
                         if(tile.Buildable && tile.Empty)
                         {
                             gridPiece.enabled = true;
                         }
                     }
                     
                     else
                     {
                         if (!tile.Empty || !tile.Buildable)
                         {
                             gridPiece.enabled = false;

                         }
                     }
                     
                 }
             }
         }
    }
}
