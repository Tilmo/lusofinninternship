using System;
using System.Collections.Generic;
using GridSystem.GridObjects;
using LevelTiles;
using UnityEditor;
using UnityEngine;
using Tile = GridSystem.GridObjects.Tile;

namespace GridSystem
{
    public class GridManager : MonoBehaviour
    {
        public FlowField.FlowField curFlowField;
        public LevelManager LevelManager { get; set; }
        public GameGrid<Tile> GameGrid { get; private set; }
        public static event EventHandler OnBuildableChange;

        [SerializeField] private bool displayGrid;
        [SerializeField] private bool debugMode;
        [SerializeField] private GridVisualizer gridVisualizer;
        
        private readonly Vector2Int _gridSize = new Vector2Int(35,35);
        private List<GridObject> _gridObjects;
        private float cellRadius = 1f;
        
                
        [SerializeField] private GridDebug debug;
        private enum GridDebug { 
            Debug1, 
            Debug2, 
            Debug3
        }
        
        public void Initialize()
        {
            _gridObjects = new List<GridObject>();
        }
        
        public void BuildGrid(int x, int z, float d)
        {
            GameGrid = new GameGrid<Tile>(x, z, d);
            
            PopulateGrid(GameGrid);
            gridVisualizer.BuildFullGrid();
            SetTilesAndUpdate();
        }
        
        public void ReadTilemapData()
        {
            foreach (Tile tile in GameGrid.Tiles)
            {
                foreach (var map in LevelManager.blockMaps)
                {
                    if (LevelManager.HazardSpot(tile.worldPos, map))
                    {
                        tile.Hazard = true;
                        break;
                    }
                }
            }
            
            InitializeFlow();
        }

        private void InitializeFlow()
        {
            var newFlow = new GridSystem.FlowField.FlowField(0.5f, new Vector2Int(35,35));
            newFlow.CreateGrid();
            newFlow.CreateCostField(GameGrid);
            
            StopCoroutine(nameof(newFlow.SetIntegrationFieldCoroutine));
            StartCoroutine(newFlow.SetIntegrationFieldCoroutine());
        }
        

        public void AddToGridObjectList(GridObject gridObject)
        {
            _gridObjects.Add(gridObject);

            InitializeFlow();
        }

        public void RemoveFromGridObjectList(GridObject gridObject)
        {
            _gridObjects.Remove(gridObject);


            InitializeFlow();
        }

        public void KillGrid()
        {
            GameGrid = null;
        }

        [ContextMenu("Kill GridObjects")]
        public void KillGridObjects()
        {
            foreach (var t in _gridObjects)
            {
                Destroy(t.gameObject);
            }
            
            _gridObjects.Clear();
        }
        
        public void HideGrid()
        {
            gridVisualizer.HideGridTiles();
        }
        
        public void RefreshBuildable()
        {
            SetTilesAndUpdate();
        }
        
        private void SetTilesAndUpdate()
        {
            OnBuildableChange?.Invoke(this, EventArgs.Empty);
            
            if(BuildingManager.BuildMode)
                UpdateGridVisual();
        }

        public void UpdateGridVisual()
        {
            gridVisualizer.UpdateGrid();
        }
        
        private void PopulateGrid(GameGrid<Tile> gameGrid)
        {
            for (var i = 0; i < gameGrid.dimensions.x; i++)
            {
                for (var j = 0; j < gameGrid.dimensions.y; j++)
                {
                    gameGrid.Tiles[i,j] = new Tile( new Vector2Int(i,j), gameGrid, null);
                }
            }
        }
        
#if UNITY_EDITOR

        private void OnDrawGizmos()
        {
            if (debugMode)
            {
                if (displayGrid)
                {
                    if (curFlowField == null)
                    {
                        DrawGrid(_gridSize, Color.yellow, cellRadius);
                    }
                    else
                    {
                        DrawGrid(_gridSize, Color.green, cellRadius);
                    }
                }
            
                if (curFlowField == null) {return;}
            
            
                GUIStyle style = new GUIStyle(GUI.skin.label);
                style.alignment = TextAnchor.MiddleCenter;

                switch (debug)
                {
                    case GridDebug.Debug1:
                        foreach (var VARIABLE in curFlowField.Grid)
                        {
                            Handles.Label(VARIABLE.worldPos, VARIABLE.cost.ToString(), style);
                        }
                        break;
                
                    case GridDebug.Debug2:
                    
                        foreach (var VARIABLE in curFlowField.Grid)
                        {
                            Handles.Label(VARIABLE.worldPos, VARIABLE.bestCost.ToString(), style);
                        }
                        break;
                
                    case GridDebug.Debug3:

                        foreach (var cell in curFlowField.Grid)
                        {
                            Handles.Label(cell.worldPos, (DirectionToString(cell.bestDirection)), style);
                        }
                        break;
                }
            }
        }

        private string DirectionToString(GridDirection direction)
        {
            if (direction == GridDirection.North)
                return "8";
            if (direction == GridDirection.NorthEast)
                return "9";
            if (direction == GridDirection.East)
                return "6";
            if (direction == GridDirection.SouthEast)
                return "3";
            if (direction == GridDirection.South)
                return "2";
            if (direction == GridDirection.SouthWest)
                return "1";
            if (direction == GridDirection.West)
                return "4";
            if (direction == GridDirection.NorthWest)
                return "7";

            return string.Empty;
        }

        private void DrawGrid(Vector2Int drawGridSize, Color drawColor, float radius)
        {
            Gizmos.color = drawColor;

            for (int x = 0; x < drawGridSize.x; x++)
            {
                for (int y = 0; y < drawGridSize.y; y++)
                {
                    Vector3 center = new Vector3( radius * 2 * x + radius, -2, radius * 2 * y + radius);
                    Vector3 size = Vector3.one * radius * 2;
                    Gizmos.DrawWireCube(center, size);
                }
            }
        }
#endif

    }
}

