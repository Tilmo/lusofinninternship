using UnityEngine;

namespace GridSystem
{
    public class GameGrid<T>
    {
        public Vector2Int dimensions;
        public readonly float diameter;
        public float Radius => diameter / 2;
        public readonly T[,] Tiles;

        public GameGrid(int x, int y, float diameter)
        {
            this.diameter = diameter;
            dimensions = new Vector2Int(x,y);
            Tiles = new T[x,y];
        }

        public Vector3 GridToWorld(int x, float y, int z) { return new Vector3(x * diameter, y, z * diameter); }
        public Vector3 GridToWorldCentered(int x, float y, int z) => GridToWorld(x, y, z) + new Vector3(diameter, y,diameter) * Radius;
        public Vector3 GridToWorldCentered(Vector3 worldPosition) => GridToWorldCentered( (int)WorldToGrid(worldPosition).x, 0, (int)WorldToGrid(worldPosition).z);
        

        public bool OnGrid(int x, int z) => NotNegative(x, z) && (InsideHeight(z) && InsideWidth(x));
    
        public bool OnGrid(Vector3 worldPosition) => OnGrid( (int)WorldToGrid(worldPosition).x, (int)WorldToGrid(worldPosition).z);
    
        public bool NotNegative(int x) => x >= 0;
        public bool NotNegative(int x, int z) => x >= 0 && z >= 0;
        public bool InsideWidth(int x) => dimensions.x > x; 
        public bool InsideHeight(int z) => dimensions.y > z;
    
        public Vector3 WorldToGrid(Vector3 worldPosition) => 
            new Vector3(Mathf.FloorToInt((worldPosition).x / diameter), (worldPosition).y,  Mathf.FloorToInt(
                (worldPosition).z / diameter));
        
        public Vector3 WorldToGrid(Vector3 worldPosition, float y)
            => new Vector3(Mathf.FloorToInt(worldPosition.x / diameter), y,  Mathf.FloorToInt(worldPosition.z / diameter));

        public T GetTile(int x, int z) => Tiles[ ClampX(x), ClampZ(z)];

        public T GetTile(Vector2Int gridIndex) => Tiles[gridIndex.x, gridIndex.y];
        
        public int ClampX(int i) => Mathf.Clamp(i, 0, dimensions.x-1);
        public int ClampZ(int i) => Mathf.Clamp(i, 0, dimensions.y-1);
        public T GetTile(Vector3 worldPosition) => OnGrid(worldPosition) ? GetTile(
            (int)WorldToGrid(worldPosition, 0).x, (int)WorldToGrid(worldPosition, 0).z) : default;
        
    }
}

