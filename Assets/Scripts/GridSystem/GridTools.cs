using Core;
using UnityEngine;

namespace GridSystem
{
    public static class GridTools
    {
        private static GridManager GridManager => GameManager.Game.GridManager;
        public static void SetBuildableTiles(int range, Transform transform)
        {
            var grid = GridManager.GameGrid;
            var list = grid.GetTile(transform.position).GetNeighbours(range, range);

            for (int j = 0; j < list.Count; j++)
            {
                if (!list[j].Buildable)
                    list[j].Buildable = true;
            }
        }

        public static void RemoveBuildableTiles(int range, Transform transform)
        {
            var grid = GridManager.GameGrid;
            var list = grid.GetTile(transform.position).GetNeighbours(range, range);

            for (int j = 0; j < list.Count; j++)
            {
                if (list[j].Buildable) 
                    list[j].Buildable = false;
            }
        }
        
        public static void RemoveBuildableTiles(int range, int x, int z)
        {
            var grid =GridManager.GameGrid;
            var list = grid.GetTile(x, z).GetNeighbours(range, range);

            for (int j = 0; j < list.Count; j++)
            {
                if (list[j].Buildable) 
                    list[j].Buildable = false;
            }
        }

        public static bool Range(int i, int j, float rangeX, float rangeZ, int x, int z)
        {
            return i > x + rangeX || i < x - rangeX || j > z + rangeZ || j < z - rangeZ;
        }
    }
}
