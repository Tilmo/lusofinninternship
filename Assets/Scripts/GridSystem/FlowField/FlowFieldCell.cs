﻿using GridSystem.GridObjects;
using UnityEngine;

namespace GridSystem.FlowField
{
    public class FlowFieldCell
    {
        public Vector3 worldPos;
        public Vector2Int gridIndex;
        public int cost;
        public int bestCost;
        public GridDirection bestDirection;

        public FlowFieldCell nextOnPath;

        public FlowFieldCell(Vector3 worldPos, Vector2Int gridIndex)
        {
            this.worldPos = worldPos;
            this.gridIndex = gridIndex;
            cost = 1;
            bestCost = int.MaxValue;
            bestDirection = GridDirection.None;
            nextOnPath = this;
        }

        public void IncreaseCost(int amt)
        {
            if (cost == int.MaxValue)
            {
                return;
            }
            cost += (amt);
        }
    }
}