﻿using System.Collections;
using System.Collections.Generic;
using Core;
using GridSystem.GridObjects;
using Humans;
using UnityEngine;

namespace GridSystem.FlowField
{
    public class FlowField
    {
        public FlowFieldCell[,] Grid { get; private set; }
        private Vector2Int GridSize { get; set; }
        private float CellRadius { get; set; }

        private readonly float _cellDiameter;
        
        public FlowField(float cellRadius, Vector2Int gridSize)
        {
            CellRadius = cellRadius;
            GridSize = gridSize;
            _cellDiameter = cellRadius * 2f;
        }

        public void CreateGrid()
        {
            Grid = new FlowFieldCell[GridSize.x, GridSize.y];

            for (var x = 0; x < GridSize.x; x++)
            {
                for (var y = 0; y < GridSize.y; y++)
                {
                    var worldPos = new Vector3(_cellDiameter * x + this.CellRadius, 0, _cellDiameter * y + CellRadius);
                    Grid[x,y] = new FlowFieldCell(worldPos, new Vector2Int(x,y));
                }
            }
        }

        public void CreateCostField( GameGrid<Tile> tiles)
        {
            for (var i = 0; i < 35; i++)
            {
                for (var j = 0; j < 35; j++)
                {
                    if(tiles.Tiles[i, j].Hazard)
                        Grid[i, j].IncreaseCost(255);

                    else if(tiles.Tiles[i, j].GridObject is Wall)
                        Grid[i,j].IncreaseCost(10);
                        
                    else if(tiles.Tiles[i, j].GridObject is Turret )
                        Grid[i,j].IncreaseCost(5);
                }
            }
        }

        private bool IsValid(Vector2Int direction)
        {
            var x = direction.x;
            var y = direction.y;
            
            return x >= 0 && y >= 0 && x < GridSize.x && y < GridSize.y && Grid[x, y].cost <= 200;
        }
        
        private bool IsValidCorner(Vector2Int direction)
        {
            
            var x = direction.x;
            var y = direction.y;
            
            return x >= 0 && y >= 0 && x < GridSize.x && y < GridSize.y && Grid[x, y].cost <= 200 && Grid[x,y].cost != 11;
        }


        private List<FlowFieldCell> GetNeighborCells(Vector2Int nodeIndex, List<GridDirection> directions)
        {
            List<FlowFieldCell> neighborCells = new List<FlowFieldCell>();

            foreach (Vector2Int curDirection in directions)
            {
                FlowFieldCell newNeighbor = GetCellAtRelativePos(nodeIndex, curDirection);

                if (newNeighbor != null)
                {
                    neighborCells.Add(newNeighbor);
                }
            }

            return neighborCells;
        }

        private List<FlowFieldCell> AllValidNeighbours(Vector2Int cellIndex)
        {
            List<FlowFieldCell> neighborCells = new List<FlowFieldCell>();
            
            var upCorner = IsValidCorner( cellIndex + GridDirection.North.Vector);
            var downCorner = IsValidCorner(cellIndex + GridDirection.South.Vector);
            var leftCorner = IsValidCorner(cellIndex + GridDirection.West.Vector);
            var rightCorner = IsValidCorner(cellIndex + GridDirection.East.Vector);
            
            var up = IsValid( cellIndex + GridDirection.North.Vector);
            var down = IsValid(cellIndex + GridDirection.South.Vector);
            var left = IsValid(cellIndex + GridDirection.West.Vector);
            var right = IsValid(cellIndex + GridDirection.East.Vector);

            if (up)
            {
                neighborCells.Add( GetCell( cellIndex + GridDirection.North.Vector ));
                
                if (rightCorner && IsValid(cellIndex + GridDirection.NorthEast.Vector))
                    neighborCells.Add( GetCell( cellIndex + GridDirection.NorthEast.Vector));
            }

            if (right)
            {
                neighborCells.Add(GetCell(cellIndex + GridDirection.East.Vector));
                
                if (downCorner && IsValid(cellIndex + GridDirection.SouthEast.Vector))
                    neighborCells.Add(GetCell(cellIndex + GridDirection.SouthEast.Vector));
            }

            if (down)
            {
                neighborCells.Add(GetCell( cellIndex + GridDirection.South.Vector));
                
                if (leftCorner && IsValid(cellIndex + GridDirection.SouthWest.Vector))
                    neighborCells.Add( GetCell(cellIndex + GridDirection.SouthWest.Vector));
            }

            if (left)
            {
                neighborCells.Add(GetCell(cellIndex + GridDirection.West.Vector));
                
                if (upCorner && IsValid(cellIndex + GridDirection.NorthWest.Vector))
                    neighborCells.Add(GetCell(cellIndex + GridDirection.NorthWest));
            }

            return neighborCells;
        }

        private FlowFieldCell GetCell(Vector2Int gridIndex) => Grid[gridIndex.x, gridIndex.y];

        private FlowFieldCell GetCellAtRelativePos(Vector2Int originPos, Vector2Int relativePos)
        {
            Vector2Int finalPos = originPos + relativePos;

            if (finalPos.x < 0 || finalPos.x >= GridSize.x || finalPos.y < 0 || finalPos.y >= GridSize.y)
            {
                return null;
            }
            
            return Grid[finalPos.x, finalPos.y];
        }

        public FlowFieldCell GetCellFromWorldPos(Vector3 worldPos)
        {
            float percentX = worldPos.x / (GridSize.x * _cellDiameter);
            float percentY = worldPos.z / (GridSize.y * _cellDiameter);

            percentX = Mathf.Clamp01(percentX);
            percentY = Mathf.Clamp01(percentY);
            
            int x = Mathf.Clamp(Mathf.FloorToInt((GridSize.x) * percentX), 0, GridSize.x -1);
            int y = Mathf.Clamp(Mathf.FloorToInt((GridSize.y) * percentY), 0, GridSize.y -1);

            return Grid[x, y];
        }

        private void ResetIntegrationField()
        {
            foreach (var variable in Grid)
            {
                variable.bestCost = 255;
            }
        }

        public IEnumerator SetIntegrationFieldCoroutine( )
        {
            var tiles = GameManager.Game.GridManager.GameGrid.Tiles;
            
            ResetIntegrationField();
            
            foreach (var tile in tiles)
            {
                if (tile.GridObject is Base || tile.GridObject is Pylon)
                    CreateIntegrationField(Grid[tile.gridIndex.x, tile.gridIndex.y]);
            }
            
            foreach (FlowFieldCell curCell in Grid)
            {
                List<FlowFieldCell> curNeighbors = AllValidNeighbours(curCell.gridIndex);
                int bestCost = curCell.bestCost;

                foreach (FlowFieldCell curNeighbor in curNeighbors)
                {
                    if (curNeighbor.bestCost < bestCost)
                    {
                        bestCost = curNeighbor.bestCost;

                        curCell.nextOnPath = curNeighbor;
                        curCell.bestDirection = GridDirection.GetDirectionFromV2I(curNeighbor.gridIndex - curCell.gridIndex);
                    }
                }
            }

            GameManager.Game.GridManager.curFlowField = this;
            yield return null;
        }
        
        private void CreateIntegrationField(FlowFieldCell destination)
        {
            destination.cost = 0;
            destination.bestCost = 0;
            
            Queue<FlowFieldCell> cellsToCheck = new Queue<FlowFieldCell>();
            
            cellsToCheck.Enqueue(destination);

            while (cellsToCheck.Count > 0)
            {
                FlowFieldCell curFlowFieldCell = cellsToCheck.Dequeue();
                List<FlowFieldCell> curNeighbors = GetNeighborCells(curFlowFieldCell.gridIndex, GridDirection.CardinalDirections);

                foreach (FlowFieldCell curNeighbor in curNeighbors)
                {
                    if (curNeighbor.cost > 200)
                    {
                        continue;
                    }

                    if (curNeighbor.cost + curFlowFieldCell.bestCost < curNeighbor.bestCost)
                    {
                        curNeighbor.bestCost = curNeighbor.cost + curFlowFieldCell.bestCost;
                        cellsToCheck.Enqueue(curNeighbor);
                    }
                }
            }

        }
    }
    

}