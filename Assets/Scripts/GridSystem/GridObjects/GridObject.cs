﻿using Health;
using StatSystem;
using UnityEngine;

namespace GridSystem.GridObjects
{
    public abstract class GridObject : MonoBehaviour
    {
        private Collider _collider;

        protected IHaveStats Stats { get; private set; }

        public IDamageable Health { get; private set; }

        public void Initialize()
        {
            Health = GetComponent<IDamageable>();
            Stats = GetComponent<IHaveStats>();
            _collider = GetComponentInChildren<Collider>();
        }
        protected void RemoveCollider()
        {
            _collider.enabled = false;
        }
    }
}