﻿using System.Collections.Generic;
using UnityEngine;

namespace GridSystem.GridObjects
{
    public class Tile
    {
        public Vector2Int gridIndex;
        public Vector3 worldPos;
        private GameGrid<Tile> GameGrid { get; }
        public GridObject GridObject { get; set; }
        public bool Buildable { get; set; }
        
        public bool Hazard { get; set; }
        public bool Empty => GridObject == null && !Hazard;

        public Tile(Vector2Int gridIndex, GameGrid<Tile> gameGrid, GridObject gridObject)
        {
            this.gridIndex = gridIndex;
            GridObject = gridObject;
            GameGrid = gameGrid;
            worldPos = WorldPositionCentered; 
        }

        public List<Tile> GetNeighbours(int rangeX, int rangeY)
        {
            var list = new List<Tile>();

            for (var i = gridIndex.x - rangeX; i < gridIndex.x + rangeX+1; i++)
            {
                for (var j = gridIndex.y - rangeY; j < gridIndex.y + rangeY+1; j++)
                {
                    if(GameGrid.OnGrid(i,j)) 
                        list.Add( GameGrid.GetTile(i,j) );
                }
            }
            return list;
        }

        public List<Tile> GetArea(int rangeX, int rangeY)
        {
            var list = new List<Tile>();

            if (gridIndex.x + rangeX > 35) rangeX = 35 - gridIndex.x;
            if (gridIndex.y + rangeY > 35) rangeY = 35 - gridIndex.y;

            for (var i = gridIndex.x; i < gridIndex.x + rangeX; i++)
            {
                for (var j = gridIndex.y; j < gridIndex.y + rangeY; j++)
                {
                    if(GameGrid.OnGrid(i,j)) 
                        list.Add( GameGrid.GetTile(i,j) );
                }
            }
            return list;
        }
        private Vector3 WorldPositionCentered => GameGrid.GridToWorldCentered(gridIndex.x, 0, gridIndex.y);
    }
    
}