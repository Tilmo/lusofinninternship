﻿using System;
using Core;

namespace GridSystem.GridObjects
{
    public abstract class GridPylon : GridObject
    {
        protected BuildingData Data { get; set; }

        protected abstract bool Ready();
        private void SetTiles()
        {
            GridTools.SetBuildableTiles(Data.buildRange, transform);
        }
        private void OnEnable()
        {
            GridManager.OnBuildableChange += GridManagerOnOnBuildableChange;
        }

        protected void OnDeath()
        {
            GridTools.RemoveBuildableTiles(Data.buildRange, transform);
            GridManager.OnBuildableChange -= GridManagerOnOnBuildableChange;
            GameManager.Game.GridManager.RefreshBuildable();
        }

        private void GridManagerOnOnBuildableChange(object sender, EventArgs e)
        {
            if(Ready()) 
                SetTiles();
        }

        private void OnDisable()
        {
            OnDeath();
        }

        private void OnDestroy()
        {
            GridManager.OnBuildableChange -= GridManagerOnOnBuildableChange;
        }
        
    }
}