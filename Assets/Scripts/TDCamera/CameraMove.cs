using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TDCamera
{
    public class CameraMove : MonoBehaviour
    {
        [SerializeField] private Vector3 origin;
        [SerializeField] private Vector3 difference;
        [SerializeField] private bool drag;
        [SerializeField] private LayerMask layerMask;

        private IRaycastHit _raycastHit;

        private void Awake()
        {
            _raycastHit = new RaycastHitRaycast(layerMask);
            drag = false;
        }

        private void OnDisable()
        {
            drag = false;
        }

        private void Update()
        {
            if(Touchscreen.current == null) return;
            
            if ((Touchscreen.current.primaryTouch.press.wasPressedThisFrame)) // && !MouseOverUI.HitUI()) )
            {
                drag = true;
                origin = _raycastHit.WorldPosition;
            }

            if (Touchscreen.current.primaryTouch.press.wasReleasedThisFrame)
            {
                drag = false;
            }

            if (drag)
            {
                HoldPosition();
            }
        }
        private void HoldPosition()
        {
            difference = origin - _raycastHit.WorldPosition;
            difference.y = 0;

            transform.position += difference;
        }
        
        
    }
}
