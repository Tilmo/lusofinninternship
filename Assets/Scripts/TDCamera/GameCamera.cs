﻿using UnityEngine;

namespace TDCamera
{
    public class GameCamera
    {
        public readonly Camera Camera;
            
        public GameCamera()
        {
            Camera = Camera.main;
        }

        public Ray ScreenToRay(Vector2 worldPosition) => Camera.ScreenPointToRay(worldPosition);
    }
}
    
