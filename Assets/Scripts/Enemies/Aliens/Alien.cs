using System;
using Health;
using UnityEngine;

namespace Enemies.Aliens
{
    public class Alien : MonoBehaviour, IAlien
    {
        public IDamageable Damageable => _iDamageable; 
        public static event EventHandler OnAlienDeath;
        
        private IDamageable _iDamageable;
        private IAgent _agent;

        [SerializeField] private Rigidbody alienRigidbody;
        [SerializeField] private Animator animator;
        [SerializeField] private float runMagnitudeLimit = 0.2f;
        
        private static readonly int IsMoving = Animator.StringToHash("isMoving");
        private static readonly int IsAttacking = Animator.StringToHash("isAttacking");

        private void FixedUpdate()
        {
            if (_agent.State == FlowFieldAgent.AgentState.Attack)
            {
                animator.SetBool(IsMoving, false );
                animator.SetBool(IsAttacking, true);
            }
            else
            {
                animator.SetBool(IsAttacking, false);
                animator.SetBool(IsMoving, alienRigidbody.velocity.magnitude > runMagnitudeLimit );
            }
        }

        public void Initialize()
        {
            _iDamageable = GetComponent<IDamageable>();
            _agent = GetComponent<IAgent>();
            
            _iDamageable.OnDeath += DamageableOnOnDeath;
        }

        private void Kill()
        {
            OnAlienDeath?.Invoke(this, EventArgs.Empty);
            Destroy(gameObject);
        }
        private void DamageableOnOnDeath()
        {
            Kill();
        }

        public void LookForTarget()
        {
            _agent?.LookForTarget();
        }

        public void GameUpdate()
        {
            _agent?.GameUpdate();
        }
    }
}
