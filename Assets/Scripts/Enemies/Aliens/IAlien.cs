﻿namespace Enemies.Aliens
{
    public interface IAlien
    {
        public Health.IDamageable Damageable { get; }
    }
}