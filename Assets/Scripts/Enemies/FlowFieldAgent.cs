﻿using Core;
using GridSystem;
using GridSystem.FlowField;
using GridSystem.GridObjects;
using StatSystem;
using UnityEngine;

namespace Enemies
{
    public class FlowFieldAgent : MonoBehaviour, IAgent
    {
        [SerializeField] private Vector3 moveDirection;
        [SerializeField] private float interpolationRatio;
        [SerializeField] private float minDistance;
        [SerializeField] private LayerMask mask;

        private Rigidbody _rigidbody;
        private GridManager _gridManager;
        private Transform _transform;
        private Transform _childTransform;
        private AlienStats _stats;
        private GridObject _target;

        private float _timer;
        
        private FlowFieldCell _flowFieldCell;
        private FlowFieldCell _lastFlowFieldCell;
        
        public enum AgentState
        {
            Seek, Attack
        }
        public AgentState agentState = default;
 

        public AgentState State => agentState;
        public void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _gridManager = GameManager.Game.GridManager;
            _transform = transform;
            _childTransform = GetComponentInChildren<Transform>();
            _stats = GetComponent<AlienStats>();

            var variance = UnityEngine.Random.Range(0.8f, 1.2f);
            
            ApplyVariance(variance);
        }

        private void ApplyVariance(float variance)
        {
            var animator = GetComponentInChildren<Animator>();
            animator.SetFloat("moveSpeed", variance);
            transform.localScale = gameObject.transform.localScale * variance ;

            _stats.attackDamage *= variance;
            _stats.moveSpeed *= variance;
        }
        public void GameUpdate()
        {
            if(agentState == AgentState.Seek) 
                Move();
            
            else if(agentState == AgentState.Attack)
                Attack();
        }

        private void Move()
        {
            if(_flowFieldCell == null)
                GetNewCell();
            
            if ( _target != null )
            {
               SetAttack();
               return;
            }
            
            if (Vector3.Distance(transform.position, _flowFieldCell.nextOnPath.worldPos) < minDistance)
            {
                GetNewCell();
            }

            if (_flowFieldCell.bestDirection == null || _flowFieldCell.bestDirection == GridDirection.None)
            {
                moveDirection = _lastFlowFieldCell.nextOnPath.worldPos - transform.position;
            }
            else
            {
                moveDirection = (_flowFieldCell.nextOnPath.worldPos - transform.position);
            }

            moveDirection.y = 0;
            
            _childTransform.forward = Vector3.Lerp(_childTransform.forward, moveDirection, interpolationRatio);

            _rigidbody.velocity = moveDirection.normalized * (_stats.MoveSpeed);
        }
        private void GetNewCell()
        {
            var newCell = _gridManager.curFlowField.GetCellFromWorldPos(_transform.position);

            if (_flowFieldCell == newCell) return;
            
            _flowFieldCell = newCell;
            _lastFlowFieldCell = _flowFieldCell;
            
            
        }
        
        public void LookForTarget()
        {
            var cell = _gridManager.curFlowField.GetCellFromWorldPos(_transform.position);

            if(Physics.Raycast( cell.worldPos, new Vector3(cell.bestDirection.Vector.x, 0, 
                cell.bestDirection.Vector.y), out var hit, _stats.AttackRange, mask, QueryTriggerInteraction.Ignore))
            {
                _target = hit.transform.root.GetComponent<GridObject>();
            }
        }
        
        private void Attack()
        {
            _rigidbody.velocity = Vector3.zero;
            
            if (_target == null)
            {
                ExitAttack();
                return;
            }
            
            if (_timer > 1 / _stats.AttackSpeed)
            {
                AttackTarget();
                _timer = 0;
            }
            else
            {
                _timer += Time.deltaTime;
            }
        }

        private void AttackTarget()
        {
            Debug.Log("Attack Got Off");
            _target.Health.TakeDamage((int)_stats.AttackDamage, DamageType.Normal);

            ExitAttack();
        }

        private void ExitAttack()
        {
            _rigidbody.mass = 3f;
            agentState = AgentState.Seek;
            
            _target = null;
            LookForTarget();
            _timer = 0;
        }


        private void SetAttack()
        {
            _rigidbody.mass = 0.25f;
            agentState = AgentState.Attack;
        }
        
    }
}