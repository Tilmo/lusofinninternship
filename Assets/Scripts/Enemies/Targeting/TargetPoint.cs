using Enemies.Aliens;
using UnityEngine;

namespace Enemies.Targeting
{
    public class TargetPoint : MonoBehaviour
    {
        public IAlien Alien { get; private set; }
        public Vector3 Position => transform.position;

        private void Awake()
        {
            Alien = transform.root.GetComponent<IAlien>();
            Debug.Assert(Alien !=null, "Target point without Alien root!", this);
            Debug.Assert(gameObject.layer == LayerMask.NameToLayer("Alien"));
        }
    }
}
