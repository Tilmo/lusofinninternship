﻿
namespace Enemies
{
    public interface IAgent
    {
        public void GameUpdate();
        public void LookForTarget();

        public FlowFieldAgent.AgentState State { get; }
    }
}