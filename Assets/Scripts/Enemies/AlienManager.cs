﻿using System;
using System.Collections;
using System.Collections.Generic;
using Core;
using Enemies.Aliens;
using UnityEngine;
using WorldMap.Blueprints;
using Random = UnityEngine.Random;

namespace Enemies
{
    public class AlienManager : MonoBehaviour
    {
        [SerializeField] private Alien meleeDrone;
        [SerializeField] private Alien rangedDrone;
        
        [SerializeField] private int aliensToSpawn = 3;
        [SerializeField] private float spawnFrequency;
        [SerializeField] private float waveFrequency;
        
        private float _timer;
        private List<Alien> _aliens;
        private bool _spawning;

        public CombatEncounter encounter;
        
        
        private void Awake()
        {
            _aliens = new List<Alien>();
        }
        
        public void Initialize()
        {
            aliensToSpawn = 3;
            _timer = 0;
            
            Alien.OnAlienDeath += AlienOnOnAlienDeath;
            // TimeTickManager.OnTimeTick += TimeTickManagerOnOnTimeTick;
            TimeTickManager.OnQuarterTimeTick += TimeTickManagerOnOnQuarterTimeTick;
        }

        private void TimeTickManagerOnOnQuarterTimeTick(object sender, EventArgs e)
        {
            if (_aliens.Count < 1) return;
            foreach (Alien alien in _aliens)
            {
                alien.LookForTarget();
            }
        }

        public void GameUpdate()
        {
            if (_aliens.Count < 1) return;

            foreach (Alien alien in _aliens)
            {
                alien.GameUpdate();
            }
        }
        

        private void TimeTickManagerOnOnTimeTick(object sender, EventArgs e)
        {
            if (_aliens.Count < 1) return;
            foreach (Alien alien in _aliens)
            {
                alien.LookForTarget();
            }
        }

        private void SpawnAlien(Vector2Int tileIndex)
        {
            if(!_spawning) return;

            Alien alien = null;
            if (UnityEngine.Random.Range(0, 2) == 0)
            {
                alien = Instantiate(meleeDrone);
            }
            else
            {
                alien = Instantiate(rangedDrone);
            }
  
            alien . transform.position = Core.GameManager.Game.GridManager.GameGrid.GetTile( tileIndex ).worldPos + Vector3.up;
            AddAlien(alien);
        }

        private void AddAlien(Alien alien)
        {
            alien.Initialize();
            _aliens.Add(alien);
        }

        public void StartSpawningAliens()
        {
            _spawning = true;
            StartCoroutine(AlienCombat());
        }
        
        private IEnumerator AlienCombat()
        {
            var time = new WaitForSeconds(waveFrequency);
            _timer = waveFrequency;

            while (_spawning)
            {
                if (_timer > waveFrequency)
                {
                    StartCoroutine(AlienWave());
                    _timer = 0;
                }
                else
                {
                    _timer += Time.deltaTime;
                }

                yield return null;
            }
        }

        private IEnumerator AlienWave()
        {
            var time = new WaitForSeconds(spawnFrequency);
            
            for (int i = 0; i < aliensToSpawn; i++)
            {
                SpawnAlien( RandomSpawnPosition() );
                yield return time;
            }

            aliensToSpawn++;
        }
        
        private void AlienOnOnAlienDeath(object sender, EventArgs e)
        {
            _aliens.Remove(sender as Alien);
        }
        
        public void KillAliens()
        {
            _spawning = false;
            
            foreach (var alien in _aliens)
            {
                Destroy(alien.gameObject);
            }
            
            _aliens.Clear();
            
            StopCoroutine(nameof(AlienCombat));
            StopCoroutine(nameof(AlienWave));
            
            
            Alien.OnAlienDeath -= AlienOnOnAlienDeath;
        }
                
        private Vector2Int position1 => new Vector2Int( 0, Random.Range(0,34));
        private Vector2Int position2 => new Vector2Int( 34, Random.Range(0,34));
        private Vector2Int position3 => new Vector2Int( Random.Range(0,34), 0);
        private Vector2Int position4 => new Vector2Int( Random.Range(0, 34), 34);

        private Vector2Int RandomSpawnPosition()
        {
            var i = Random.Range(1, 5);

            return i switch
            {
                1 => position1,
                2 => position2,
                3 => position3,
                _ => position4
            };
        }
        
    }
}