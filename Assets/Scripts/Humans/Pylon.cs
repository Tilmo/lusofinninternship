﻿using System;
using GridSystem.GridObjects;
using Resources;
using StatSystem;

namespace Humans
{
    public class Pylon : GridPylon
    {
        public static event Action PylonBuilt;
        private Tile _tile;

        public void Initialize(Tile tile, BuildingData data )
        {
            _tile = tile;
            Data = data;
        
            HumanStats humanStats = (HumanStats) Stats;
            humanStats.Initialize(data);

            transform.position = _tile.worldPos;
        
            Health.OnDeath += Kill;
        }
        
        public void Kill()
        {
            ResourceManager.Instance.FreeHumans( Data.humanCost );
            ResourceManager.Instance.RemoveHumans( Data.humanCost );
        
            _tile.GridObject = null;
            
            RemoveCollider();
            OnDeath();
        
            Core.GameManager.Game.GridManager.RemoveFromGridObjectList(this);
            Destroy(gameObject);
        }
        public void Arm()
        {
            _armed = true;
            PylonBuilt?.Invoke();
        }
        private bool _armed;

        protected override bool Ready()
        {
            return Data != null && _armed;
        }
        private void OnDisable()
        {
            Health.OnDeath -= Kill;
        }
    }
}