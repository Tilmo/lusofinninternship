﻿using System;
using System.Collections.Generic;
using Core;
using UnityEngine;

namespace Humans
{
    public class TurretManager : MonoBehaviour
    {
        private readonly List<ITurret> _turrets = new List<ITurret>();

        private void Awake()
        {
            TimeTickManager.OnTimeTick += TimeTickManagerOnOnTimeTick;
            Turret.TurretArmEvent += TurretOnTurretArmEvent;
        }

        private void TurretOnTurretArmEvent(Turret.TurretEventArgs obj)
        {
            if (obj.arm)
            {
                Arm(obj.turret);
                return;
            }
             
            Disarm(obj.turret);
        }

        public void GameUpdate()
        {
            foreach (var turret in _turrets)
            {
                turret.AttackCooldown();
            }
        }
        
        private void TimeTickManagerOnOnTimeTick(object sender, EventArgs e)
        {
            foreach (var turret in _turrets)
            {
                turret.SeekTarget();
            }
        }

        private void Arm(ITurret turret)
        {
            _turrets.Add(turret);
        }

        private void Disarm(ITurret turret)
        {
            _turrets.Remove(turret);
        }




    }
}