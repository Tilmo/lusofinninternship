﻿using System;
using Core;
using GridSystem;
using GridSystem.GridObjects;
using Resources;

namespace Humans
{
    public class Base : GridObject
    { 
        public Tile Tile { get; set; }

        private void OnEnable()
        {
            GridManager.OnBuildableChange += GridManagerOnOnBuildableChange;
            TimeTickManager.OnTimeTick += TimeTickManagerOnOnTimeTick;
        }

        private void GridManagerOnOnBuildableChange(object sender, EventArgs e)
        {
            SetTiles();
        }
        
        private void TimeTickManagerOnOnTimeTick(object sender, EventArgs e)
        {
            ResourceManager.Instance.AddMetal(50);
            ResourceManager.Instance.AddResearch(10);
        }
    
        private void OnDisable()
        {
            TimeTickManager.OnTimeTick -= TimeTickManagerOnOnTimeTick;
            GridManager.OnBuildableChange -= GridManagerOnOnBuildableChange;
        }

        private void SetTiles()
        {
            GridTools.SetBuildableTiles(6, transform);
        }
    }
}