﻿using Enemies.Targeting;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Humans
{
    public class ManualTargeting : MonoBehaviour
    {
        [SerializeField] private bool targetingEnabled;
        
        private IRaycastHit _raycastHit;
        
        private void Awake()
        {
            _raycastHit = new RaycastHitRaycast();
        }

        private void Update()
        {
            if(!targetingEnabled) return;
            if(Touchscreen.current == null) return;
            if (!Touchscreen.current.press.wasPressedThisFrame) return;
            if(_raycastHit == null) return;
            if(MouseOverUI.HitUI()) return;
            
            
            var targetPoint = _raycastHit.RaycastHit.collider.GetComponent<TargetPoint>();

            if (targetPoint != null)
            {
                var tile = Core.GameManager.Game.GridManager.GameGrid.GetTile(targetPoint.Position);
                var list = tile.GetNeighbours(10, 10);

                foreach (var t in list)
                {
                    if (!(t.GridObject is Turret a)) continue;
                    
                    
                    a.SetWeaponTarget(targetPoint);
                    a.SeekTarget();
                }
            }
            else
            {
                CheckGridObject();
            }
        }

        private void CheckGridObject()
        {
            var tile = Core.GameManager.Game.GridManager.GameGrid.GetTile(_raycastHit.WorldPosition);

            var gridObject = tile?.GridObject;

            if (gridObject != null)
            {
                tile?.GridObject.GetComponent<ConstructionController>()?.CancelConstruction(); 
            }
         
        }
    }
}