﻿namespace Humans
{
    public interface ITurret
    {
        public void AttackCooldown();
        public void SeekTarget();
    }
}