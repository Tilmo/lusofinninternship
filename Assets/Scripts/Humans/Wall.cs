using Core;
using GridSystem.GridObjects;
using StatSystem;

namespace Humans
{
    public class Wall : GridObject
    { 
        public void Initialize(BuildingData data)
        {
            HumanStats humanStats = (HumanStats) Stats;
            humanStats.Initialize(data);

            Health.OnDeath += Kill;
        }
        
        public void Kill()
        {
            var tile = Core.GameManager.Game.GridManager.GameGrid.GetTile(transform.position);
        
            tile.GridObject = null;
            GameManager.Game.GridManager.LevelManager.DestroyWall(tile.worldPos);

            RemoveCollider();
            
            Core.GameManager.Game.GridManager.RefreshBuildable();
            Core.GameManager.Game.GridManager.RemoveFromGridObjectList(this);
            
            Destroy(gameObject);
        }

        private void OnDisable()
        {
            if(Health == null) return;
        
            Health.OnDeath -= Kill;
        }
    
    }
}