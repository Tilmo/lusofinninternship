﻿using System;
using Enemies.Targeting;
using GridSystem.GridObjects;
using StatSystem;
using UnityEngine;
using Weapons;

namespace Humans
{
    public class Turret : GridObject, ITurret
    {
        public Core.DamageType WeaponDamageType => _weapon.DamageType;
        public float WeaponTimerPercentage => _timer / ( 1 / Stats.AttackSpeed);
        public event Action OnWeaponFire;
        
        [SerializeField] private Transform firePoint;

        private Transform _rotatingTurret;
        private IWeapon _weapon;
        private float _timer;
        private TargetPoint _targetPoint;
        
        public void Initialize(BuildingData data)
        {
            HumanStats humanStats = (HumanStats) Stats;
            humanStats.Initialize(data);

            _weapon = GetComponent<IWeapon>();
            _weapon.FirePoint = firePoint;
            
            Health.OnDeath += Kill;

            _rotatingTurret = transform.Find("Turret");
        }

        public static event Action<TurretEventArgs> TurretArmEvent; 

        public void Arm()
        {
            TurretArmEvent?.Invoke( new TurretEventArgs(this, true));
        }

        public class TurretEventArgs : EventArgs
        {
            public readonly bool arm;
            public readonly Turret turret;
            public TurretEventArgs(Turret turret, bool arm)
            {
                this.arm = arm;
                this.turret = turret;
            }
        }
        public void SeekTarget()
        {
            _targetPoint = _weapon.TargetFinder.Target;
        }

        public void SetWeaponTarget(TargetPoint targetPoint)
        {
            _weapon.TargetFinder.Target = targetPoint;
        }

        public void AttackCooldown()
        {
            OnWeaponFire?.Invoke();

            if (_targetPoint != null)
            {
                var lookPoint = _targetPoint.Position;
                lookPoint.y = 0;
                _rotatingTurret.LookAt(lookPoint);
                
                if (_timer > 1 / Stats.AttackSpeed)
                {
                    _weapon.Fire();
                    
                    _timer = 0;
                }
                else
                {
                    _timer += Time.deltaTime;
                }
            }
            else
            {
                _timer = 0;
            }
        }

        private void OnDestroy()
        {
            TurretArmEvent?.Invoke( new TurretEventArgs(this, false));
        }

        public void Kill()
        {
            var tile = Core.GameManager.Game.GridManager.GameGrid.GetTile(transform.position);
        
            tile.GridObject = null;

            RemoveCollider();
            
            Core.GameManager.Game.GridManager.RefreshBuildable();
            Core.GameManager.Game.GridManager.RemoveFromGridObjectList(this);
            
            Destroy(gameObject);
        }
        
        
#region Unity Functions
#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if(_targetPoint == null) return;
            Gizmos.DrawLine(transform.position, _targetPoint.Position);
        }
#endif
#endregion
        
    }
}
