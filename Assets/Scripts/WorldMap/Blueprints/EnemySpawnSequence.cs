﻿using System;
using Core.Data;
using UnityEngine;

namespace WorldMap.Blueprints
{
    [Serializable]
    public class EnemySpawnSequence
    {
        public AlienType alienType;

        [Range(1, 100)] 
        public int amount = 1;

        [Range(0.1f, 10f)]
        public float cooldown = 1f;
    }
}