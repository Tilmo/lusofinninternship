﻿using System;
using UnityEngine;

namespace WorldMap.Blueprints
{
    public class SimpleMapBuilder : MonoBehaviour
    {
        private static int RandomMapTemplateIndex(MapTemplate[] templates) => UnityEngine.Random.Range(0, templates.Length);

        public static MapTemplateLayer[] BuildMap(MapTemplate[] mapTemplates, EncounterCollection encounterCollection)
        {
            var randomIndex = RandomMapTemplateIndex(mapTemplates);

            var template = Instantiate(mapTemplates[randomIndex]);

            var map = new MapTemplateLayer[ template.layers.Length ];
            
            Array.Copy(template.layers, map, template.layers.Length);

            var value = (map[ map.Length -1].x) / 5f; 
            
            for (int x = 0; x < map.Length; x++)
            {
                var layer = map[x];
                for (int i = 0; i < map[x].templateNodes.Length ; i++)
                {
                    var node = map[x].templateNodes[i];
                    node.x = x;
                    
                    if (node.difficulty == 0)
                    {
                        node.difficulty = Mathf.CeilToInt(layer.x / value ) ;
                    }

                    if (node.nodeType == NodeType.Encounter)
                    {
                        node.encounterIndex = encounterCollection.GetRandomCombatEncounterIndex(node.difficulty);

                        node.map = MapCollection.MapList[UnityEngine.Random.Range(0, MapCollection.MapList.Length)];
                    }
                    else 
                    {
                        node.encounterIndex = encounterCollection.GetRandomPointOfInterestIndex();
                    }
                    
                }
            }
            return map;
        }
        
    }
}