﻿using UnityEngine;

namespace WorldMap.Blueprints
{
    public class MapTemplateTester : MonoBehaviour
    {
        public MapTemplate template;
        public float radius;


        public Color baseColor;
        public Color poiColor;
        public Color combatColor;
        public Color startColor;
        public Color bossColor;

        public Color linkColor;
        public Color linkEndColor;

        private bool SetColorAlphas()
        {
            baseColor.a = 225;
            poiColor.a = 225;
            combatColor.a = 225;
            startColor.a = 255;
            bossColor.a = 255;
            linkColor.a = 255;
            linkEndColor.a = 255;

            return true;
        }
        public Color TypeToColor(NodeType nodeType)
        {
            if (nodeType == NodeType.Encounter)
            {
                return combatColor;
            }

            if (nodeType == NodeType.PointOfInterest)
            {
                return poiColor;
            }

            if (nodeType == NodeType.Start)
            {
                return startColor;
            }

            if (nodeType == NodeType.BossEncounter)
            {
                return bossColor;
            }

            return baseColor;
        }
        public static Color StateToColor(NodeState nodeState)
        {
            if (nodeState == NodeState.Current)
            {
                return Color.yellow;
            }
            if (nodeState == NodeState.Available)
            {
                return Color.blue;
            }

            if (nodeState == NodeState.Locked)
            {
                return Color.gray;
            }
            if (nodeState == NodeState.Cleared)
            {
                return Color.black;
            }
            return Color.red;
        }

        private bool _initialized;
        private void OnDrawGizmos()
        {
            if(template == null) return;
            
            if (!_initialized)
            {
                _initialized = SetColorAlphas();;
            }
           
            for (int x = 0; x < template.layers.Length; x++)
            {
                var layer = template.layers[x];
                
                for (int i = 0; i < template.layers[x].templateNodes.Length; i++)
                {
                    var node = template.layers[x].templateNodes[i];

                    Gizmos.color = TypeToColor(node.nodeType);
                    
                    Gizmos.DrawSphere(new Vector2(layer.x + node.offsetX, node.y + node.offsetY), radius);
                }
            }

            for (int x = 0; x < template.layers.Length; x++)
            {
                var layer = template.layers[x];
                for (int i = 0; i < template.layers[x].templateNodes.Length; i++)
                {
                    var node = template.layers[x].templateNodes[i];

                    foreach (var link in node.links)
                    {
                        var startPos = new Vector2(layer.x + node.offsetX, node.y + node.offsetY);

                        var linksLayer = template.layers[link.layerIndex];
                        var linksNode = linksLayer.templateNodes[link.nodeIndex];

                        var endPos = new Vector2(linksLayer.x + linksNode.offsetX, linksNode.y + linksNode.offsetY);
                        var endArrowPos = endPos;
                        endPos -= (endPos -startPos).normalized * (radius * 1.2f);
                        Gizmos.color = linkColor;
                        Gizmos.DrawLine(startPos, endPos);
                        
                        Gizmos.color = linkEndColor;
                        Gizmos.DrawLine(endPos, endArrowPos);
                    }
                }

            }
        }
    }

}