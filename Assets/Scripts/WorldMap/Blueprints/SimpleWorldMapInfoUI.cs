﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace WorldMap.Blueprints
{
    public class SimpleWorldMapInfoUI : MonoBehaviour
    {
        public static SimpleWorldMapInfoUI Instance => _instance;
        private static SimpleWorldMapInfoUI _instance;

        public Image image;
        public TMP_Text mapName;
        public TMP_Text encounterName;
        public TMP_Text difficulty;
        private void Awake()
        {
            _instance = this;
        }

        public void DisplayPicture(Sprite sprite)
        {
            image.sprite = sprite;
        }

        public void DisplayInfo(string newMapName, string newEncounterName, string newDifficulty)
        {
            this.mapName.text = newMapName;
            this.encounterName.text = newEncounterName;
            this.difficulty.text = newDifficulty;
        }
       

        
    }
}