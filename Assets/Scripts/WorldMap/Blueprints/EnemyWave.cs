﻿using System;
using System.Collections;
using UnityEngine;

namespace WorldMap.Blueprints
{
    [Serializable]
    [CreateAssetMenu(menuName = "Create EnemyWave", fileName = "EnemyWave", order = 0)]
    public class EnemyWave : ScriptableObject
    {
        public EnemySpawnSequence[] spawnSequences = {new EnemySpawnSequence()};
        
        [Range(0.1f, 10f)]
        public float cooldown = 1f;

        public IEnumerator SpawnSequence()
        {
            var sequenceIndex = 0;

            while (sequenceIndex < spawnSequences.Length)
            {
                var spawnedAmount = 0;
                var time = new WaitForSeconds( spawnSequences[sequenceIndex].cooldown );
                while (spawnedAmount < spawnSequences[sequenceIndex].amount )
                {
                    yield return time;
                    //spawn Alien
                    spawnedAmount++;
                }

                yield return new WaitForSeconds(cooldown);
                sequenceIndex++;
            }
        }
    }
}