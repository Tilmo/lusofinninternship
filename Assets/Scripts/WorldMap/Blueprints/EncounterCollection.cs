﻿namespace WorldMap.Blueprints
{
    public class EncounterCollection
    {
        private readonly CombatEncounter[] _combatEncounters1;
        private readonly CombatEncounter[] _combatEncounters2;
        private readonly CombatEncounter[] _combatEncounters3;
        private readonly CombatEncounter[] _combatEncounters4;
        private readonly CombatEncounter[] _combatEncounters5;
        
        private readonly CombatEncounter[] _combatEncounters6;

        private readonly PointOfInterest[] _pointOfInterests;
        public CombatEncounter[] GetEncounters(int difficulty)
        {
            if (difficulty == 1)
            {
                return _combatEncounters1;
            }
            if (difficulty == 2)
            {
                return _combatEncounters2;
            }
            if (difficulty == 3)
            {
                return _combatEncounters3;
            }
            if (difficulty == 4)
            {
                return _combatEncounters4;
            }
            if (difficulty == 5)
            {
                return _combatEncounters5;
            }

            return _combatEncounters6;
        }

        public int GetRandomCombatEncounterIndex(int difficulty)
        {
            if (difficulty == 1)
            {
                return UnityEngine.Random.Range(0, _combatEncounters1.Length);
            }
            if (difficulty == 2)
            {
                return UnityEngine.Random.Range(0, _combatEncounters2.Length);
            }
            if (difficulty == 3)
            {
                return UnityEngine.Random.Range(0, _combatEncounters3.Length);
            }
            if (difficulty == 4)
            {
                return UnityEngine.Random.Range(0, _combatEncounters4.Length);
            }
            if (difficulty == 5)
            {
                return UnityEngine.Random.Range(0, _combatEncounters5.Length);
            }

            return -1;
        }
        
        public int GetRandomPointOfInterestIndex()
        {
            return UnityEngine.Random.Range(0, _pointOfInterests.Length);
        }

        public PointOfInterest GetPointOfInterest(int index)
        {
            return _pointOfInterests[index];
        }

        public CombatEncounter GetCombatEncounter(int difficulty, int index)
        {
            var encounters = GetEncounters(difficulty);
            return encounters[index];
        }

        public EncounterCollection()
        {
            var encounters1 = UnityEngine.Resources.LoadAll<CombatEncounter>($"CombatEncounters/CombatEncounters1");
            _combatEncounters1 = new CombatEncounter[encounters1.Length];
            _combatEncounters1 = encounters1;
            
            _combatEncounters2 = UnityEngine.Resources.LoadAll<CombatEncounter>($"Encounters/CombatEncounters/CombatEncounters2");
            _combatEncounters3 = UnityEngine.Resources.LoadAll<CombatEncounter>($"Encounters/CombatEncounters/CombatEncounters3");
            _combatEncounters4 = UnityEngine.Resources.LoadAll<CombatEncounter>($"Encounters/CombatEncounters/CombatEncounters4");
            _combatEncounters5 = UnityEngine.Resources.LoadAll<CombatEncounter>($"Encounters/CombatEncounters/CombatEncounters5");

            _combatEncounters6 = UnityEngine.Resources.LoadAll<CombatEncounter>($"Encounters/CombatEncounters/CombatEncounters6");
            
            _pointOfInterests = UnityEngine.Resources.LoadAll<PointOfInterest>($"Encounters/PointOfInterests");
        }
    }
    
    public static class MapCollection
    {
        public static readonly string[] MapList = { "level1", "level2", "level3" };
 
    }
}