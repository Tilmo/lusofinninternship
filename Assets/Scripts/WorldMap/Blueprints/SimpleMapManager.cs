﻿using System;
using System.Collections.Generic;
using Resources;
using Serialization;
using UnityEngine;
using UnityEngine.InputSystem;

namespace WorldMap.Blueprints
{
    public class SimpleMapManager : MonoBehaviour
    {
        public static SimpleMapManager Instance => _instance;
        private static SimpleMapManager _instance;

        private MapTemplate[] _mapTemplates;
        public MapTemplateLayer[] map;
        public MapTemplateNode selected;

        public EncounterCollection encounterCollection;

        [SerializeField] private EncounterManager encounterManager;
        public MapTemplateNode CurrentNode { get; set; }

        private Transform _transform;
        
        private void Awake()
        {
            _instance = this;
            _mapTemplates = UnityEngine.Resources.LoadAll<MapTemplate>("MapTemplates");
            
            encounterCollection = new EncounterCollection();
            
            EncounterManager.EncounterComplete += EncounterManagerOnEncounterComplete;

            _transform = transform;
        }

        private void EncounterManagerOnEncounterComplete()
        {
            EndEncounter();
        }

        [ContextMenu("Encounter End Event")]
        public void EndEncounter()
        {
            foreach (var layer in map)
            {
                foreach (var node in layer.templateNodes)
                {
                    if (node.nodeState == NodeState.Current)
                    {
                        node.nodeState = NodeState.Cleared;
                        continue;
                    }
                    
                    if(node.nodeState == NodeState.Cleared) continue;
                    node.nodeState = NodeState.Locked;
                }
            }

            if (CurrentNode.nodeType == NodeType.BossEncounter)
            {
                if (Resources.ResourceManager.Instance != null)
                {
                    SaveData.Current.humansAmount = 100;
                    SaveData.Current.metalAmount = 150;
                    SaveData.Current.researchAmount =
                        Resources.ResourceManager.Instance.resourceWorldSession.ResearchAmount;
                
                    ResourceManager.Instance.StartNewWorldSession();
                }

                BuildMap();
                return;
            }
            
            CurrentNode.nodeState = NodeState.Current;
            
            foreach (var link in CurrentNode.links)
            {
                map[link.layerIndex].templateNodes[link.nodeIndex].nodeState = NodeState.Available;
            }
        }

        public void ResetMap()
        {
            if (Resources.ResourceManager.Instance != null)
            {
                SaveData.Current.humansAmount = 100;
                SaveData.Current.metalAmount = 150;
                SaveData.Current.researchAmount =
                    Resources.ResourceManager.Instance.resourceWorldSession.ResearchAmount;
                
                ResourceManager.Instance.StartNewWorldSession();
            }

            BuildMap();
            return;
        }

        private void Update()
        {
            if (Mouse.current.leftButton.wasPressedThisFrame)
            {
                if (Camera.main is { } && Physics.Raycast(Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue()),
                    out RaycastHit hit))
                {
                    var go = hit.transform.GetComponent<MapNodeBehaviour>();

                    if(go == null) return;
                    
                    if (go.Node.nodeState == NodeState.Available)
                    {
                        if(selected.nodeState != NodeState.Current) 
                            selected.nodeState = NodeState.Available;
                        
                        go.Node.nodeState = NodeState.Selected;
                        
                        selected = go.Node;

                        
                        if (selected.nodeType == NodeType.Encounter)
                        {
                            
                            SimpleWorldMapInfoUI.Instance.DisplayInfo( selected.map, 
                                encounterCollection.GetCombatEncounter(selected.difficulty, selected.encounterIndex).name, selected.difficulty.ToString() );

                            
                            var image = UnityEngine.Resources.Load<Sprite>($"MapImages/{selected.map}");
                            
                            SimpleWorldMapInfoUI.Instance.DisplayPicture(image);
                        }

                        else if (selected.nodeType == NodeType.PointOfInterest)
                        {
                            SimpleWorldMapInfoUI.Instance.DisplayInfo(string.Empty, string.Empty, 
                                encounterCollection.GetPointOfInterest(selected.encounterIndex).description);

                            var image = UnityEngine.Resources.Load<Sprite>("MapImages/poi");
                            SimpleWorldMapInfoUI.Instance.DisplayPicture(image);
                        }

                        else
                        {
                            SimpleWorldMapInfoUI.Instance.DisplayInfo(string.Empty, string.Empty, 
                                string.Empty);
                            
                            SimpleWorldMapInfoUI.Instance.DisplayPicture(null);
                        }
                    }
          
                }
            }
        }

        [ContextMenu("Build Map")]
        public void BuildMap()
        {
            map = SimpleMapBuilder.BuildMap(_mapTemplates, encounterCollection);

            foreach (var layer in map)
            {
                foreach (var node in layer.templateNodes)
                {
                    if (node.nodeState != NodeState.Current) continue;
                    CurrentNode = node;
                    break;
                }
            }
            VisualizeMap();
        }

        
        [ContextMenu("Build Map from Save")]
        public void BuildFromSave()
        {
            LoadMap();
            
            foreach (var layer in map)
            {
                foreach (var node in layer.templateNodes)
                {
                    if (node.nodeState != NodeState.Selected) continue;
                    node.nodeState = NodeState.Available;
                    break;
                }
                
                foreach (var node in layer.templateNodes)
                {
                    if (node.nodeState != NodeState.Current) continue;
                    CurrentNode = node;
                    break;
                }
            }
            VisualizeMap();
            
        }
        
        [ContextMenu("Save Map")]
        private void SaveMap()
        {
            SaveData.Current.worldMapSave = map;
        }
        
        private void LoadMap()
        {
            if(SaveData.Current.worldMapSave != null) 
                map = SaveData.Current.worldMapSave;

            else
            {
                map = SimpleMapBuilder.BuildMap(_mapTemplates, encounterCollection);
            }
        }

        [ContextMenu("Travel To Selected")]
        public void TravelToSelected()
        {
            MapTraversal(selected);
        }

        private void MapTraversal(MapTemplateNode node)
        {
            CurrentNode = node;
            encounterManager.ProcessEncounter(CurrentNode, encounterCollection);
        }
        
        public MapNodeBehaviour nodeTemplate;
        public LineRenderer lineRenderer;

        private readonly List<GameObject> _gameObjects = new List<GameObject>();
        private void VisualizeMap()
        {
            foreach (var go in _gameObjects)
            {
                Destroy(go);
            }
            
            _gameObjects.Clear();

            for (int x = 0; x < map.Length; x++)
            {
                var layer = map[x];
                
                for (int i = 0; i < map[x].templateNodes.Length; i++)
                {
                    var node = map[x].templateNodes[i];

                    var go = Instantiate(nodeTemplate, _transform);
                    
                    go.transform.position = new Vector2(layer.x + node.offsetX, node.y + node.offsetY);

                    go.Node = node;
                    go.ColorUpdate();
                    
                    _gameObjects.Add(go.gameObject);
                }
            }

            for (int x = 0; x < map.Length; x++)
            {
                var layer = map[x];
                for (int i = 0; i < map[x].templateNodes.Length; i++)
                {
                    var node = layer.templateNodes[i];

                    foreach (var link in node.links)
                    {
                        var startPos = new Vector3(layer.x + node.offsetX, node.y + node.offsetY);

                        var linksLayer = map[link.layerIndex];
                        var linksNode = linksLayer.templateNodes[link.nodeIndex];

                        var endPos = new Vector3(linksLayer.x + linksNode.offsetX, linksNode.y + linksNode.offsetY); 
                        

                        var line = Instantiate(lineRenderer, _transform);
                        line.SetPosition(0, startPos );
                        line.SetPosition(1, endPos );
                        
                        _gameObjects.Add(line.gameObject);
                    }
                }
            }
        }
    }
    
    
}