﻿using UnityEngine;

namespace WorldMap.Blueprints
{
    public class DefaultPositionSetter : MonoBehaviour
    {
        [SerializeField] private Vector3 position;
        private void OnEnable()
        {
            ResetPosition();
        }
        
        [ContextMenu("Reset")]

        private void ResetPosition()
        {
            transform.position = position;
        }
    }
}