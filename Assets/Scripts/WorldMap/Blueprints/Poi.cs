﻿using System;

namespace WorldMap.Blueprints
{
    [Serializable]
    public struct Poi
    {
        public Resource resource;
        public int amount;
    }
}