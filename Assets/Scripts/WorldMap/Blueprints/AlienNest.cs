﻿using System;

namespace WorldMap.Blueprints
{
    [Serializable]
    public class AlienNest 
    {
        public EnemyWave[] waves;
        public SpawnType spawnType;
    }
}