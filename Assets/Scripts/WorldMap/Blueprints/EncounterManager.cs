﻿using System;
using Core;
using Health;
using LevelLoadSystem;
using Resources;
using UnityEngine;

namespace WorldMap.Blueprints
{
    public class EncounterManager : MonoBehaviour
    {
        public static event Action EncounterComplete;
        public event Action EncounterFailed;
        
        [SerializeField] private LevelLoader loader;
        [SerializeField] private GameManager gameManager;

        [SerializeField] private bool debug;

        private void Awake()
        {
            HumansAsDamageable.OutOfHumans += HumansAsDamageableOnOutOfHumans;
        }

        private void HumansAsDamageableOnOutOfHumans()
        {
            EncounterFailed?.Invoke();
        }

        public void ProcessEncounter(MapTemplateNode node, EncounterCollection encounterCollection)
        {
            if (!debug)
            {
                if (node.nodeType == NodeType.Encounter)
                {
                    loader.SelectLevel( new Level(node.map));
                    gameManager.StatGame();
                }

                if (node.nodeType == NodeType.PointOfInterest)
                {
                    var poi = encounterCollection.GetPointOfInterest(node.encounterIndex);
                
                    ProcessPoi(poi);
                }
            }

            EncounterComplete?.Invoke();
        }
        
        
        private void ProcessPoi(PointOfInterest pointOfInterest)
        {
            if (ResourceManager.Instance != null)
            {
                
                foreach (var poi in pointOfInterest.give)
                {
                    PoiGive( poi );
                }

                foreach (var poi in pointOfInterest.take)
                {
                    PoiTake( poi );
                }
            }
        }
        private void PoiGive( Poi poi )
        {
            if(poi.resource == Resource.None)
                return;
            
            if (poi.resource == Resource.Humans)
            {
                Resources.ResourceManager.Instance.AddHumansWorld( poi.amount );
            }
            if (poi.resource == Resource.Metal)
            {
                Resources.ResourceManager.Instance.AddMetalWorld( poi.amount );
            }
            if (poi.resource == Resource.Research)
            {
                Resources.ResourceManager.Instance.AddResearchWorld( poi.amount );
            }
        }

        private void PoiTake(Poi poi)
        {
            if(poi.resource == Resource.None)
                return;
            
            if (poi.resource == Resource.Humans)
            {
                Resources.ResourceManager.Instance.RemoveHumansWorld( poi.amount );
            }
            if (poi.resource == Resource.Metal)
            {
                Resources.ResourceManager.Instance.RemoveMetalWorld( poi.amount );
            }
            if (poi.resource == Resource.Research)
            {
                Resources.ResourceManager.Instance.RemoveResearchWorld( poi.amount );
            }
        }
    }
}