﻿using System;
using UnityEngine;

namespace WorldMap.Blueprints
{
    [Serializable]
    [CreateAssetMenu(fileName = "MapTemplate", menuName = "MapTemplate", order = 0)]
    public class MapTemplate : ScriptableObject
    {
        public MapTemplateLayer[] layers;
    }

    [Serializable]
    public class MapTemplateLayer
    {
        public int x;
        public MapTemplateNode[] templateNodes;
    }

    [Serializable]
    public class MapTemplateNode
    {
        [HideInInspector] public int x;
        public int y;
        public float offsetX;
        public float offsetY;

        public int difficulty;
        public string map;
        public int encounterIndex;

        public NodeType nodeType;
        public NodeState nodeState;
        
        public MapTemplateLink[] links;
    }

    [Serializable]
    public class MapTemplateLink
    {
        public int layerIndex;
        public int nodeIndex;
    }

    [Serializable]
    public enum NodeType
    {
        Encounter, PointOfInterest, BossEncounter, Start
    }

    [Serializable]
    public enum NodeState
    {
        Locked, Current, Available, Cleared, Selected
    }
}