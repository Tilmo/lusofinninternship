﻿using UnityEngine;

namespace WorldMap.Blueprints
{
    [CreateAssetMenu(menuName = "Create CombatEncounter", fileName = "CombatEncounter", order = 0)]
    public class CombatEncounter : ScriptableObject
    {
        public AlienNest[] alienNests;
        
        [HideInInspector] public Vector3[] spawnPositions;
    }

    public enum SpawnType
    {
        Wave, Endless
    }
}