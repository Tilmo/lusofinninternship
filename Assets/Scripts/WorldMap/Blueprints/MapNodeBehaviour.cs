﻿using UnityEngine;

namespace WorldMap.Blueprints
{
    public class MapNodeBehaviour : MonoBehaviour
    {
        public MapTemplateNode Node { get; set; }

        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private SpriteRenderer _border;

        public void ColorUpdate()
        {
            _nodeState = Node.nodeState;
            _spriteRenderer.color = MapTemplateTester.StateToColor(Node.nodeState);

            if (Node.nodeType == NodeType.Start)
            {
                _border.color = Color.white;
            }
            if (Node.nodeType == NodeType.Encounter)
            {
                _border.color = Color.black;
            }

            if (Node.nodeType == NodeType.PointOfInterest)
            {
                _border.color = Color.yellow;
            }

            if (Node.nodeType == NodeType.BossEncounter)
            {
                _border.color = Color.black;
            }

            if (Node.nodeState == NodeState.Selected)
            {
                _border.color = Color.clear;
                _border.transform.localScale = _border.transform.localScale * 1.5f;
            }
        }

        private NodeState _nodeState;
        private void Update()
        {
            if(Node == null) return;

            if (_nodeState != Node.nodeState)
            {
                _spriteRenderer.color = MapTemplateTester.StateToColor(Node.nodeState);

                _nodeState = Node.nodeState;
            }
        }
    }
}