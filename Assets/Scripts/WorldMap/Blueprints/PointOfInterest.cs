using UnityEngine;

namespace WorldMap.Blueprints
{
    [CreateAssetMenu]
    public class PointOfInterest : ScriptableObject
    {
        public Poi[] give;
        public Poi[] take;

        public string description;
        public string flavourText;
    }

    public enum Resource
    {
        None, Metal, Humans, Research
    }
}