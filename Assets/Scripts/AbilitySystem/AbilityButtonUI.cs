﻿using System;
using System.Collections;
using System.Globalization;
using Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace AbilitySystem
{
    public class AbilityButtonUI : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private TMP_Text text;
        
        private Button _button;
        private WaitForSeconds _seconds;
        private bool _assigned;
        private void Awake()
        {
            _button = GetComponent<Button>();
            _seconds = new WaitForSeconds(1f);
        }
        public void Interactable()
        {
            if (_assigned)
                _button.interactable = false;
        }
        public void StartCooldownCoroutine(float time)
        {
            StartCoroutine(CooldownCoroutine(time));
        }

        public void ResetButton()
        {
            _button.interactable = true;
           ResetButtonText();
           ResetListeners();
        }

        private void ResetButtonText()
        {
            text.text = string.Empty;
        }

        private void ResetListeners()
        {
           _button.onClick.RemoveAllListeners();
        }
        private IEnumerator CooldownCoroutine(float f)
        {
            var i = 0;
            text.text = (f - i).ToString(CultureInfo.InvariantCulture);
            
            while (i < f)
            {
                yield return _seconds;
                i++;
                text.text = (f - i).ToString(CultureInfo.InvariantCulture);
            }

            _button.interactable = true;
            text.text = string.Empty;
        }

        public string ability = string.Empty;

        public void UpdateIcon(Sprite sprite)
        {
            icon.sprite = sprite;
        }
        public void UpdateButton()
        {
            if(ability == string.Empty) return;

            _assigned = true;

            if (ability == nameof(GameManager.Game.AbilityController.HealAbility))
            {
                _button.onClick.AddListener( GameManager.Game.AbilityController.HealAbility );
                return;
            }
            
            if (ability == nameof(GameManager.Game.AbilityController.DamageAbility))
            {
                _button.onClick.AddListener( GameManager.Game.AbilityController.DamageAbility );
                return;
            }
            
            if (ability == nameof(GameManager.Game.AbilityController.BloodLustAbility))
            {
                _button.onClick.AddListener( GameManager.Game.AbilityController.BloodLustAbility );
                return;
            }
            
            if (ability == nameof(GameManager.Game.AbilityController.CrippleAbility))
            {
                _button.onClick.AddListener( GameManager.Game.AbilityController.CrippleAbility );
            }
        }
    }
}