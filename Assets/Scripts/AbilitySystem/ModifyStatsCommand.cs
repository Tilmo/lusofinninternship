﻿using Core;
using StatSystem;

namespace AbilitySystem
{
    public class ModifyStatsCommand : Command
    {
        private IHaveStats _stats;
        private Stat _stat;
        private float _amount;
        private float _time;
        private bool _multiply;

        public ModifyStatsCommand(IHaveStats stats, Stat stat, float amount, float time, bool multiply=true)
        {
            _stats = stats;
            _stat = stat;
            _amount = amount;
            _time = time;
            _multiply = multiply;
        }
        
        public override void Execute()
        {
           if(_multiply)
               _stats.MultiplyStat(_stat, _amount, _time);
           
           else
               _stats.ModifyStat(_stat, _amount, _time);
        }

        public override void Undo()
        {
            
        }

        public override bool IsFinished { get; }
    }
}