﻿using Core;
using Health;
using Humans;
using StatSystem;
using UnityEngine;

namespace AbilitySystem
{
    public class AbilityController : MonoBehaviour
    {
        public int skillPoints = 2;
        public LayerMask humanLayerMask;
        public LayerMask alienLayerMask;

        [SerializeField] private Transform previewTransform;

        [SerializeField] private AbilityButtonUI[] buttons;
        [SerializeField] private GameObject[] selectAbilityGameObjects;
        
        [SerializeField] private Sprite healIcon;
        [SerializeField] private Sprite damageIcon;
        [SerializeField] private Sprite crippleIcon;
        [SerializeField] private Sprite bloodLustIcon;

        [SerializeField] private Sprite defaultIcon;

        private readonly CommandProcessor _processor = new CommandProcessor();
        private int _buttonIndex = 0;
        

        public void StartGame()
        {
            skillPoints = 0;
            _buttonIndex = 0;
            
            Pylon.PylonBuilt += PylonOnPylonBuilt;
            
            foreach (var go in selectAbilityGameObjects)
            {
                go.SetActive(true);
            }
        }

        public void EndGame()
        {
            Pylon.PylonBuilt -= PylonOnPylonBuilt;
            
            foreach (var button in buttons)
            {
                button.UpdateIcon(defaultIcon);
                button.ResetButton();
            }
        }

        private void PylonOnPylonBuilt()
        {
            skillPoints++;
        }
        
        public void SelectHeal()
        {
            if (skillPoints < 1 || _buttonIndex == buttons.Length) return;
            
            buttons[_buttonIndex].UpdateIcon(healIcon);
                
            buttons[_buttonIndex].ability = nameof(HealAbility);
            buttons[_buttonIndex].UpdateButton();
            _buttonIndex++;
            skillPoints--;
        }
        
        public void SelectDamage()
        {
            if (skillPoints < 1 || _buttonIndex == buttons.Length) return;
            
            buttons[_buttonIndex].UpdateIcon(damageIcon);
                
            buttons[_buttonIndex].ability =  nameof(DamageAbility);
            buttons[_buttonIndex].UpdateButton();
            _buttonIndex++;
            skillPoints--;
        }

        public void SelectCripple()
        {
            if (skillPoints < 1 || _buttonIndex == buttons.Length) return;
            
            buttons[_buttonIndex].UpdateIcon(crippleIcon);
                
            buttons[_buttonIndex].ability =  nameof(CrippleAbility);
            buttons[_buttonIndex].UpdateButton();
            _buttonIndex++;
            skillPoints--;
        }

        public void SelectBloodLust()
        {
            if (skillPoints < 1 || _buttonIndex == buttons.Length) return;
            
            buttons[_buttonIndex].UpdateIcon(bloodLustIcon);
                
            buttons[_buttonIndex].ability =  nameof(BloodLustAbility);
            buttons[_buttonIndex].UpdateButton();
            _buttonIndex++;
            skillPoints--;
        }
        public void GameUpdate()
        {
            _processor.ProcessCommands();
        }
        public void HealAbility()
        {
            var numTargets = GetTargets(humanLayerMask, previewTransform.position);
            for (int i = 0; i < numTargets; i++)
            {
                var health = _targetStatBuffer[i].transform.root.GetComponent<IDamageable>();
                _processor.AddCommand(new HealCommand(health,30));
            }

            foreach (var button in buttons)
            {
                if (button.ability == nameof(HealAbility))
                {
                    button.StartCooldownCoroutine(5f);
                }
            }
        }
        public void DamageAbility()
        {
            var numTargets = GetTargets(alienLayerMask, previewTransform.position);
            for (int i = 0; i < numTargets; i++)
            {
                var health = _targetStatBuffer[i].transform.root.GetComponent<IDamageable>();
                _processor.AddCommand( new DamageCommand(health, 30, DamageType.Normal));
            }
            
            foreach (var button in buttons)
            {
                if (button.ability == nameof(DamageAbility))
                {
                    button.StartCooldownCoroutine(5f);
                }
            }
        }
        public void BloodLustAbility()
        {
            var numTargets = GetTargets(humanLayerMask, previewTransform.position);
            for (int i = 0; i < numTargets; i++)
            {
                var a = _targetStatBuffer[i].transform.root.GetComponent<IHaveStats>();
                _processor.AddCommand(new ModifyStatsCommand(a, Stat.AttackSpeed, 40f, 60f));
            }
            
            foreach (var button in buttons)
            {
                if (button.ability == nameof(BloodLustAbility))
                {
                    button.StartCooldownCoroutine(5f);
                }
            }
        }
        public void CrippleAbility()
        {
            var numTargets = GetTargets(alienLayerMask, previewTransform.position);

            for (int i = 0; i < numTargets; i++)
            {
                var a = _targetStatBuffer[i].transform.root.GetComponent<IHaveStats>();
                _processor.AddCommand(new ModifyStatsCommand(a, Stat.AttackSpeed, -25f, 5f));
                _processor.AddCommand(new ModifyStatsCommand(a, Stat.MoveSpeed, -25f, 5f));
            }
            
            foreach (var button in buttons)
            {
                if (button.ability == nameof(CrippleAbility))
                {
                    button.StartCooldownCoroutine(5f);
                }
            }
        }

        private readonly Collider[] _targetStatBuffer = new Collider[20];
        private int GetTargets(LayerMask layerMask, Vector3 position)
        {
            Vector3 a = position;
            Vector3 b = a;
            b.y += 2f;
            float r = 0.75f;
            
            int hits = Physics.OverlapCapsuleNonAlloc(a, b, r, _targetStatBuffer, layerMask);

            return hits;
        }
    }
    
    
}