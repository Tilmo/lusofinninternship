﻿using Core;
using Health;

namespace AbilitySystem
{
    public class DamageCommand : Command
    {
        private readonly IDamageable _target;
        private readonly DamageType _damageType;
        private readonly int _amount;

        public DamageCommand(IDamageable target, int amount, DamageType damageType)
        {
            _target = target;
            _amount = amount;
            _damageType = damageType;
        }
        public override void Execute()
        {
            _target.TakeDamage(_amount, _damageType);
        }

        public override void Undo()
        {
            _target.HealDamage(_amount);
        }

        public override bool IsFinished { get; }
    }
}