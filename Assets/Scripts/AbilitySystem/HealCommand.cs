﻿using Core;
using Health;

namespace AbilitySystem
{
    public class HealCommand : Command
    {
        private readonly IDamageable _target;
        private readonly int _amount;

        public HealCommand(IDamageable target, int amount)
        {
            _target = target;
            _amount = amount;
        }
        public override void Execute()
        {
            _target.HealDamage(_amount);
        }

        public override void Undo()
        {
            _target.TakeDamage(_amount);
        }

        public override bool IsFinished { get; }
    }
}