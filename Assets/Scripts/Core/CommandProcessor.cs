using System.Collections.Generic;
using System.Linq;

namespace Core
{
    public class CommandProcessor
    {
        private Command _currentCommand;
        private static Queue<Command> _commands;
        private readonly Stack<Command> _commandHistory;

        public CommandProcessor()
        {
            _commands  = new Queue<Command>();
            _commandHistory = new Stack<Command>();
        }

        public void AddCommand(Command command)
        {
            _commands.Enqueue(command);
            _commandHistory.Push( command );
        }
        
        public void ProcessCommands()
        {
            if(_commands.Any() == false)
                return;
            
            _currentCommand = _commands.Dequeue();
            _currentCommand.Execute();
        }
    }
}