﻿using System;
using System.Collections;
using UnityEngine;

namespace Core
{
    public class TimeTickManager : MonoBehaviour
    {
        public static event EventHandler OnTimeTick;
        public static event EventHandler OnQuarterTimeTick;
        
        [SerializeField] private float timePerTick;
        [SerializeField] private bool ticking;
        
        [ContextMenu("Start Timer")]
        public void StartTimer()
        {
            ticking = true;
            StartCoroutine(TimeTick( new WaitForSeconds(timePerTick)));
            StartCoroutine(QuarterTimeTick(new WaitForSeconds(timePerTick / 4)));
        }

        public void StopTimer()
        {
            ticking = false;
        }

        private IEnumerator TimeTick(WaitForSeconds seconds)
        {
            while (ticking)
            {
                yield return seconds;
                OnTimeTick?.Invoke(this, EventArgs.Empty);
            }
        }
        
        private IEnumerator QuarterTimeTick(WaitForSeconds seconds)
        {
            while (ticking)
            {
                yield return seconds;
                OnQuarterTimeTick?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}