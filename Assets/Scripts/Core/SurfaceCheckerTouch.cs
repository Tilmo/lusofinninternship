﻿using TDCamera;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Core
{
    public class SurfaceCheckerTouch 
    {
        public bool Found { get; private set; }
        public RaycastHit hit;
        
        private readonly GameCamera _gameCamera;
        private readonly LayerMask _layerMask = ~0;

        public void Check() => Found = Physics.Raycast(_gameCamera.ScreenToRay( Touchscreen.current.primaryTouch.position.ReadValue() ), out hit, 1000,_layerMask);
        public SurfaceCheckerTouch(GameCamera gameCamera)
        {
            _gameCamera = gameCamera;
        }
        public SurfaceCheckerTouch(GameCamera gameCamera, LayerMask layerMask)
        {
            _gameCamera = gameCamera;
            _layerMask = layerMask;
        }
        
    }
}