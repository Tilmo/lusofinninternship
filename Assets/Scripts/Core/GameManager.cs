﻿using System.Collections;
using AbilitySystem;
using Enemies;
using GridSystem;
using Humans;
using LevelLoadSystem;
using Resources;
using Serialization;
using UI.Menus;
using UnityEngine;
using WorldMap.Blueprints;

namespace Core
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private Vector3 gridDimensions;
        [SerializeField] private GridManager gridManager;
        [SerializeField] private BuildingManager buildingManager;
        [SerializeField] private ResourceManager resourceManager;
        [SerializeField] private TimeTickManager timeManager;
        [SerializeField] private LevelLoader levelLoader;
        [SerializeField] private TurretManager turretManager;
        [SerializeField] private AlienManager alienManager;
        [SerializeField] private AbilityController abilityController;
        [SerializeField] private EncounterManager encounterManager;

        [SerializeField] private Vector3 startingPosition;
        [SerializeField] private bool playing;

        [SerializeField] private SaveManager saveManager;

        public static GameComponents Game;
        private void Awake()
        {
            if (gridManager == null)
                gridManager = GetComponentInChildren<GridManager>();

            if (buildingManager == null)
                buildingManager = GetComponentInChildren<BuildingManager>();

            if (resourceManager == null)
                resourceManager = GetComponentInChildren<ResourceManager>();

            if (timeManager == null)
                timeManager = GetComponentInChildren<TimeTickManager>();

            DebugComponentAsserts();
            gridManager.Initialize();
            buildingManager.Initialize();


            Game = new GameComponents(gridManager, buildingManager, abilityController);
        }

        private void DebugComponentAsserts()
        {
            Debug.Assert(gridManager != null, "No Grid Manager");
            Debug.Assert(buildingManager != null, "No Building Manager");
            Debug.Assert(resourceManager != null, "No Resource Manager");
            Debug.Assert(timeManager != null, "No Time Manager");
            Debug.Assert(levelLoader != null, "No Level Loader");
            Debug.Assert(turretManager != null, "No Turret Manager");
            Debug.Assert(abilityController != null, "No Ability Controller");
        }

        private void Start()
        {
            saveManager.Load();
            resourceManager.Initialize();
        }

        [ContextMenu("Start Game")]
        public void StatGame()
        {
            if (playing) return;
            
            StartCoroutine(GameStartCoroutine());
        }

        IEnumerator GameStartCoroutine()
        {
            PageController.Instance.TurnPageOff(PageType.WorldMenu, PageType.Loading);
            
            gridManager.BuildGrid((int) gridDimensions.x, (int) gridDimensions.y, gridDimensions.z);
            buildingManager.StartGame(startingPosition);
            resourceManager.StartGame();
            abilityController.StartGame();
            levelLoader.LoadLevel();
            
            encounterManager.EncounterFailed += EncounterManagerOnEncounterFailed;

            alienManager.Initialize();

            while (!levelLoader.LoadingDone)
            {
                yield return new WaitForSeconds(1.5f);
            }

            gridManager.ReadTilemapData();

            playing = true;
            PageController.Instance.TurnPageOn(PageType.BuildingMenu);
            PageController.Instance.TurnPageOn(PageType.AbilityMenu);

            PageController.Instance.TurnPageOff(PageType.Loading, PageType.GameOverlay);

            resourceManager.InitializeUI();
        }

        private void EncounterManagerOnEncounterFailed()
        {
            EndGame();
            WorldMap.Blueprints.SimpleMapManager.Instance.ResetMap();
            
            encounterManager.EncounterFailed -= EncounterManagerOnEncounterFailed;
        }

        private void Update()
        {
            if (playing)
            {
                buildingManager.GameUpdate();
                alienManager.GameUpdate();
                abilityController.GameUpdate();
                turretManager.GameUpdate();
            }
        }

        public void EndGame()
        {
            StartCoroutine(EndGameCoroutine());
        }

        private IEnumerator EndGameCoroutine()
        {
            PageController.Instance.TurnPageOff(PageType.GameOverlay, PageType.Loading);
            playing = false;

            resourceManager.EndSession();

            PageController.Instance.TurnPageOff(PageType.BuildingMenu);
            PageController.Instance.TurnPageOff(PageType.AbilityMenu);

            buildingManager.DestroyButtons();

            gridManager.HideGrid();
            gridManager.KillGridObjects();
            gridManager.KillGrid();

            abilityController.EndGame();

            alienManager.KillAliens();

            timeManager.StopTimer();

            levelLoader.UnloadLevel();

            yield return new WaitForSeconds(1.5f);
            PageController.Instance.TurnPageOff(PageType.Loading, PageType.WorldMenu);

            resourceManager.InitializeWorldMapUI();
        }

        public void TickTimer()
        {
            timeManager.StartTimer();
        }
    }
}