﻿using AbilitySystem;
using GridSystem;
using Humans;

namespace Core
{
    public class GameComponents
    {
        public GridManager GridManager { get; }
        public BuildingManager BuildingManager { get; }
        public AbilityController AbilityController { get; }
        public GameComponents(GridManager gridManager, BuildingManager buildingManager, AbilityController abilityController)
        {
            GridManager = gridManager;
            BuildingManager = buildingManager;
            AbilityController = abilityController;

        }
    }
}