using JetBrains.Annotations;

namespace Core
{
    [CanBeNull]
    public abstract class Command
    {
        public abstract void Execute();
        public abstract void Undo();
        public abstract bool IsFinished { get; }
    }
}
