namespace Core
{
    public enum DamageType
    {
        Normal,
        ArmorPiercing, 
        Healing
    };
}
