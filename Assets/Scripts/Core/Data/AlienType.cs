﻿namespace Core.Data
{
    public enum AlienType
    {
        MeleeDrone, RangeDrone, BioMortar, Rhino, Phaser, HiveTitan
    }
}