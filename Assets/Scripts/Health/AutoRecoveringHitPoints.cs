﻿using System;
using Core;
using GridSystem;
using GridSystem.GridObjects;
using Humans;
using UnityEngine;

namespace Health
{
    public class AutoRecoveringHitPoints : MonoBehaviour, IDamageable
    {
        public event Action OnHpChange; public event Action OnDeath;
        
        [SerializeField] private int health;
        [SerializeField] private int totalHealth;
        [SerializeField] private int healPercentage;
  
        private Tile _tile;
        
        private float HealTick => (totalHealth * healPercentage  * 0.01f);
        private int _ticksToHeal;
        private void OnEnable()
        {
            TimeTickManager.OnTimeTick += TimeTickManagerOnOnTimeTick;
            _tile = GameManager.Game.GridManager.GameGrid.GetTile(transform.position);
        }

        private void OnDisable()
        {
            TimeTickManager.OnTimeTick -= TimeTickManagerOnOnTimeTick;
        }

        private void TimeTickManagerOnOnTimeTick(object sender, EventArgs e)
        {
            if(health == totalHealth) return;
            if(_tile.GridObject as Pylon) return;

            if (_ticksToHeal <= 0)
            {
                _ticksToHeal++;
                return;
            }
            HealDamage( Mathf.CeilToInt(HealTick) );
        }
        
        public int TotalHealth
        {
            get => totalHealth;
            set => totalHealth = Mathf.Clamp(value, 0, int.MaxValue);
        }
        public int Health
        {
            get => health;
            set => health = Mathf.Clamp(value, 0, totalHealth );
        }

        public void Initialize(int hp, int totalHp, int recoveryPercentage, int buildTime)
        {
            this.Health = hp;
            this.TotalHealth = totalHp;
            healPercentage = recoveryPercentage;
            _ticksToHeal = buildTime;
        }

        private void OnValidate()
        {
            TotalHealth = totalHealth;
            Health = health;
        }

        [ContextMenu("Heal To Full")]
        public void HealToFull()
        {
            HealDamage(totalHealth);
        }
        
        public void TakeDamage(int damage, DamageType damageType=DamageType.Normal)
        {
            Health -= damage;
            
            if(Health <= 0)
                OnDeath?.Invoke();

            _ticksToHeal = -4;
            
            OnHpChange?.Invoke();
        }

        public void HealDamage(int heal)
        {
            Health += heal;
            
            OnHpChange?.Invoke();
        }
    }
}