﻿using System;
using Core;
using UnityEngine;

namespace Health
{
    public class ArmoredHitPoints : MonoBehaviour, IDamageable
    {
        public event Action OnDeath;
        public event Action OnHpChange;
        
        [Range(0,100)]
        [SerializeField] private int damageReduction;
        [SerializeField] private int health;
        [SerializeField] private int totalHealth;
        public void HealDamage(int heal)
        {
            Health += heal;
            OnHpChange?.Invoke();
        }

        public int TotalHealth
        {
            get => totalHealth;
            set => totalHealth = Mathf.Clamp(value, 0, int.MaxValue);
        }
        public int Health
        {
            get => health;
            set => health = Mathf.Clamp(value, 0, totalHealth );
        }

        private int ArmorPercentage
        {
            get => damageReduction;
            set => damageReduction = Mathf.Clamp(value, 0, 100);
        }
        
        public void SetHealth(int hp, int totalHp, int armorPercentage)
        {
            this.Health = hp;
            this.TotalHealth = totalHp;
            this.ArmorPercentage = armorPercentage;
        }

        private void OnValidate()
        {
            TotalHealth = totalHealth;
            Health = health;
        }

        [ContextMenu("Heal To Full")]
        public void HealToFull()
        {
            health = totalHealth;
            OnHpChange?.Invoke();
        }

        public void TakeDamage(int damage, DamageType damageType=DamageType.Normal)
        {
            if (damageType == DamageType.ArmorPiercing)
            {
                Health -= damage;
            }
            else
            {
                var i = Mathf.CeilToInt( damage - (damage * (damageReduction * .01f)));
                Health -= Mathf.Clamp(i, 1, damage);
            }

            if(Health <= 0)
                OnDeath?.Invoke();
            
            OnHpChange?.Invoke();
        }
    }
}