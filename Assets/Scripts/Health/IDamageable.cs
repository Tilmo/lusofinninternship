using System;
using Core;

namespace Health
{
    public interface IDamageable
    {
        public int Health { get; set; }
        public int TotalHealth { get; set; }
        public void TakeDamage(int damage, Core.DamageType damageType=DamageType.Normal);
        public void HealDamage(int healing);
        public event Action OnDeath;
        public event Action OnHpChange;
    }
}
