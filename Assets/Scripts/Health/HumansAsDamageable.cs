﻿using System;
using Resources;
using UnityEngine;

namespace Health
{
    public class HumansAsDamageable : MonoBehaviour, IDamageable
    {
        public event Action OnDeath;
        public event Action OnHpChange;

        public static event Action OutOfHumans;
        public int TotalHealth { get; set; }
        
        public int Health
        {
            get => ResourceManager.Instance.HumanAmount;

            set => ResourceManager.Instance.AddHumans(value);
        }
        
        public void TakeDamage(int damage, Core.DamageType damageType= Core.DamageType.Normal)
        {
            ResourceManager.Instance.RemoveHumans(1);

            if (Health <= 0)
            {
                OnDeath?.Invoke();
                OutOfHumans?.Invoke();
            }
               
        }

        public void HealDamage(int healing)
        {
            ResourceManager.Instance.AddHumans(1);
        }
        
    }
}