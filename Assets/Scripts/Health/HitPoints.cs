﻿using System;
using Core;
using UnityEngine;

namespace Health
{
    public class HitPoints : MonoBehaviour, IDamageable
    {
        [SerializeField] private int health;
        [SerializeField] private int totalHealth;
        public event Action OnHpChange; public event Action OnDeath;

        public int TotalHealth
        {
            get => totalHealth;
            set => totalHealth = Mathf.Clamp(value, 0, int.MaxValue);
        }
        public int Health
        {
            get => health;
            set => health = Mathf.Clamp(value, 0, totalHealth );
        }

        public void SetHealth(int h, int t)
        {
            this.health = h;
            this.totalHealth = t;
        }

        private void OnValidate()
        {
            TotalHealth = totalHealth;
            Health = health;
        }

        [ContextMenu("Heal To Full")]
        public void HealToFull()
        {
            HealDamage(totalHealth);
        }
        
        public void TakeDamage(int damage, Core.DamageType damageType=DamageType.Normal)
        {
            Health -= damage;
            
            if(Health <= 0)
                OnDeath?.Invoke();
            
            OnHpChange?.Invoke();
        }

        public void HealDamage(int heal)
        {
            Health += heal;
            
            OnHpChange?.Invoke();
        }
    }
}