﻿namespace StatSystem
{
    public enum Stat
    {
        None, AttackDamage, MoveSpeed, AttackSpeed, AttackRange
    }
}