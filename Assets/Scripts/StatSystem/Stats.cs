﻿using System.Collections;
using UnityEngine;
namespace StatSystem
{
    public abstract class Stats : MonoBehaviour, IHaveStats
    {
        public float AttackDamage => Mathf.Max(0, (BaseAttackDamage + modifiers.AttackDamageModifier) * (1 + multipliers.AttackDamageMultiplier / 100) );
        public float MoveSpeed => Mathf.Max(0, (BaseMoveSpeed + modifiers.MoveSpeedModifier)* (1 + multipliers.MoveSpeedMultiplier / 100) );
        public float AttackSpeed => Mathf.Max(0,(BaseAttackSpeed + modifiers.AttackSpeedModifier) * (1 + multipliers.AttackSpeedMultiplier / 100)) ;
        public float AttackRange => Mathf.Max(0, (BaseAttackRange + modifiers.AttackRangeModifier) * (1 + multipliers.AttackSpeedMultiplier / 100));

        protected abstract float BaseAttackDamage { get; }
        protected abstract float BaseMoveSpeed { get; }

        protected abstract float BaseAttackSpeed { get; }
        protected abstract float BaseAttackRange { get; }
        
        public StatModifiers modifiers = new StatModifiers();
        public StatMultipliers multipliers = new StatMultipliers();
        
        public void ModifyStat(Stat stat, float change, float time)
        {
            switch (stat)
            {
                case Stat.None:
                    return;
                case Stat.AttackDamage:
                    modifiers.AttackDamageModifier += change;
                    break;
                case Stat.AttackSpeed:
                    modifiers.AttackSpeedModifier += change;
                    break;
                case Stat.AttackRange:
                    modifiers.AttackRangeModifier += change;
                    break;
                case Stat.MoveSpeed:
                    modifiers.MoveSpeedModifier += change;
                    break;
            }

            if(time > 0) 
                StartCoroutine(ReturnModifiedStats(new WaitForSeconds(time), stat, change));
        }

        public void MultiplyStat(Stat stat, float change, float time)
        {
            switch (stat)
            {
                case Stat.None:
                    return;
                case Stat.AttackDamage:
                    multipliers.AttackDamageMultiplier += change;
                    break;
                case Stat.AttackSpeed:
                    multipliers.AttackSpeedMultiplier += change;
                    break;
                case Stat.AttackRange:
                    multipliers.AttackRangeMultiplier += change;
                    break;
                case Stat.MoveSpeed:
                    multipliers.MoveSpeedMultiplier += change;
                    break;
            }

            if(time > 0) 
                StartCoroutine(ReturnMultipliedStats(new WaitForSeconds(time), stat, change));
        }


        private IEnumerator ReturnModifiedStats(WaitForSeconds waitForSeconds, Stat stat, float change)
        {
            yield return waitForSeconds;
            
            ModifyStat(stat, -(change), 0);
        }

        private IEnumerator ReturnMultipliedStats(WaitForSeconds waitForSeconds, Stat stat, float change)
        {
            yield return waitForSeconds;
            
            MultiplyStat(stat, -(change), 0);
        }
        
        
    }
}