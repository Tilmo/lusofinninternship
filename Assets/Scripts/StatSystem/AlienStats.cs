﻿using UnityEngine;

namespace StatSystem
{
    public class AlienStats : Stats
    {
        protected override float BaseAttackDamage => attackDamage;
        protected override float BaseMoveSpeed => moveSpeed;
        protected override float BaseAttackSpeed => attackSpeed;
        protected override float BaseAttackRange => attackRange;

        public float attackDamage;
        public float moveSpeed;
        public float attackSpeed;
        public float attackRange;
    }
}