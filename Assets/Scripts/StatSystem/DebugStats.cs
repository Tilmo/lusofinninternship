﻿using UnityEngine;

namespace StatSystem
{
    public class DebugStats : Stats
    {
        protected override float BaseAttackDamage => attackDamage;
        protected override float BaseMoveSpeed => moveSpeed;
        protected override float BaseAttackSpeed => attackSpeed;
        protected override float BaseAttackRange => attackRange;
        
        [SerializeField] private float attackDamage;
        [SerializeField] private float attackSpeed;
        [SerializeField] private float attackRange;
        [SerializeField] private float moveSpeed;
    }
}