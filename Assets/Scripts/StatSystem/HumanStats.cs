﻿namespace StatSystem
{
    public class HumanStats : Stats
    {
        protected override float BaseAttackDamage => _stats.attackDamage;
        protected override float BaseMoveSpeed => _stats.moveSpeed;
        protected override float BaseAttackSpeed => _stats.attackSpeed;
        protected override float BaseAttackRange => _stats.attackRange;

        private BuildingData _stats;

        
        public void Initialize(BuildingData statsObject)
        {
            _stats = statsObject;
            modifiers = new StatModifiers();
            multipliers = new StatMultipliers();
        }
    }
}