﻿namespace StatSystem
{
    public class StatMultipliers
    {
        public float AttackDamageMultiplier { get; set; }

        public float AttackRangeMultiplier { get; set; }

        public float AttackSpeedMultiplier { get; set; }

        public float MoveSpeedMultiplier { get; set; }
    }
}