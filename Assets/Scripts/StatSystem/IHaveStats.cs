namespace StatSystem
{
    public interface IHaveStats
    {
        public float AttackDamage { get; }
        public float MoveSpeed { get; }
        public float AttackSpeed { get; }
        public float AttackRange { get; }

        public void ModifyStat(Stat stat, float change, float time);
        public void MultiplyStat(Stat stat, float change, float time);
    }
}

