﻿namespace StatSystem
{
    public class StatModifiers
    {
        public float AttackDamageModifier { get; set; }

        public float AttackRangeModifier { get; set; }

        public float AttackSpeedModifier { get; set; }

        public float MoveSpeedModifier { get; set; }
    }
}