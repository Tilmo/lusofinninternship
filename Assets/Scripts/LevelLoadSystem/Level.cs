﻿using UnityEngine;

namespace LevelLoadSystem
{
    public class Level
    {
        public Level(string name)
        {
            sceneName = name;
        }
        public readonly string sceneName;
    }
}