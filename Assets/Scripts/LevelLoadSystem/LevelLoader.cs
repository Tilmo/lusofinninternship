﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LevelLoadSystem
{
    public class LevelLoader : MonoBehaviour
    {
        public bool LoadingDone => _currentLoad.isDone;

        private Level _currentLevel;
        private AsyncOperation _currentLoad;

        private readonly Stack<Level> _loadedLevels = new Stack<Level>();
        private readonly Stack<Scene> _loadedScenes = new Stack<Scene>();

        [ContextMenu("Load Map Scene")]
        private void LoadWorldMap()
        {
            SceneManager.LoadSceneAsync("WorldMapTest", LoadSceneMode.Additive);
        }
        public void SelectLevel(Level level)
        {
            _currentLevel = level;
        }
        public void LoadLevel(string name)
        {
            _currentLevel = new Level(name);
            _currentLoad = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
            _loadedLevels.Push(_currentLevel);
        }
        
        public void LoadLevel()
        {
            _currentLoad = SceneManager.LoadSceneAsync(_currentLevel.sceneName, LoadSceneMode.Additive);
            _loadedLevels.Push(_currentLevel);
        }
        public void LoadScene(Scene scene)
        {
            _loadedScenes.Push( SceneManager.GetSceneByName(scene.name));
        }
        public void UnloadLevel()
        {
            if (!_loadedLevels.Any()) return;
            
            var level = _loadedLevels.Pop();
            SceneManager.UnloadSceneAsync(level.sceneName);
        }

    }
}